1
00:00:00,000 --> 00:00:04,000
Atëherë, kemi ca aplikacione hapur, një film që po luhet,

2
00:00:05,000 --> 00:00:11,000
Tetris duke u luajtur në cep, multi-tasking dhe mandej, krejt papritur,

3
00:00:12,000 --> 00:00:14,000
KWin-i ynë vithiset.

4
00:00:17,000 --> 00:00:22,000
Dhe audioja nuk pushoi, Tetris po vazhdon si më parë, sapo të kalojë rrëmuja.

5
00:00:26,000 --> 00:00:29,000
Dhe IDE-ja jonë është ende e përdorshme.

6
00:00:29,000 --> 00:00:33,000
Dhe mund ta bëj këtë shumë herë, të simuloj vithisje të shumta,

7
00:00:37,000 --> 00:00:39,000
dhe s’humbëm një kuadro.

8
00:00:40,000 --> 00:00:45,000
Mund të kopjojmë madje ca tekst, këtë, të rinisim KWin-in.

9
00:00:46,000 --> 00:00:49,000
Dhe ja, gjendet ende te tabela ime e grumbullimeve.

10
00:00:52,000 --> 00:00:53,000
I fortë.
