1
00:00:00,000 --> 00:00:04,000
Bien, tenemos algunas aplicaciones abiertas: reproducción de películas,

2
00:00:05,000 --> 00:00:11,000
jugamos al Tetris en la esquina, multitarea... Y, de repente,

3
00:00:12,000 --> 00:00:14,000
nuestro KWin falla.

4
00:00:17,000 --> 00:00:22,000
Y el sonido no se detiene, el Tetris continúa en cuanto se recupera...

5
00:00:26,000 --> 00:00:29,000
Y nuestro IDE sigue estando disponible.

6
00:00:29,000 --> 00:00:33,000
Y puedo ejecutar esto varias veces, simular múltiples fallos,

7
00:00:37,000 --> 00:00:39,000
y no perdemos ni un fotograma.

8
00:00:40,000 --> 00:00:45,000
Incluso podemos copiar texto, copiar esto, reiniciar KWin.

9
00:00:46,000 --> 00:00:49,000
Y ahora todavía sigue estando en mi tablero de recortes.

10
00:00:52,000 --> 00:00:53,000
Es robusto.
