1
00:00:00,000 --> 00:00:04,880
Ainsi, nous commençons donc dans un environnement Plasma fiable.

2
00:00:04,880 --> 00:00:10,480
Nous avons « CSGO » ouvert, en haut et cette petite application personnalisée que j'ai faite.

3
00:00:10,480 --> 00:00:13,720
Cliquez sur un bouton et hop, nous sommes dans Weston.

4
00:00:13,720 --> 00:00:15,240
Toutes les applications ont survécu.

5
00:00:15,240 --> 00:00:17,040
Ceci n'est aucune couche supplémentaire.

6
00:00:17,040 --> 00:00:20,880
Ce sont ces applications déplacées entre les compositeurs.

7
00:00:20,880 --> 00:00:23,120
Cliquez sur un bouton et retournez sous Plasma.

8
00:00:23,120 --> 00:00:28,320
Et de façon intéressante, nous sommes passés des décorations du côté client à celles côté serveur et inversement.

9
00:00:28,320 --> 00:00:32,520
Et de nombreuses différentes versions et différences sont gérées implicitement dans le

10
00:00:32,520 --> 00:00:33,520
Boîte à outils.

11
00:00:33,520 --> 00:00:36,800
Nous pouvons passer à Gnome.

12
00:00:36,800 --> 00:00:38,760
Commencez dans ce bel effet de vue d'ensemble.

13
00:00:38,760 --> 00:00:43,040
Tous mes clients sont toujours là, conservant leur taille.

14
00:00:43,040 --> 00:00:44,040
Peut aller à Hyprland.

15
00:00:44,040 --> 00:00:47,640
Attendez que cela se charge.

16
00:00:47,640 --> 00:00:53,440
Accédez à une très belle animation quand c'est le cas. Waouh !

17
00:00:53,440 --> 00:00:56,120
Nous pouvons continuer à nous déplacer autour de « CSGO ».

18
00:00:56,120 --> 00:00:57,760
Il se comporte comme avant.

19
00:00:57,760 --> 00:00:59,400
Tout fonctionne correctement.

20
00:00:59,400 --> 00:01:00,400
Passons à Sway.

21
00:01:00,400 --> 00:01:04,959
C'est la seule façon dont j'ai compris comment lancer une application dans Sway.

22
00:01:04,959 --> 00:01:07,280
Les fenêtres sont toujours là.
