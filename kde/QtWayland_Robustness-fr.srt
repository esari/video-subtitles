1
00:00:00,000 --> 00:00:04,000
Ok, ainsi, nous avons des applications qui s'ouvrent, des films qui passent,

2
00:00:05,000 --> 00:00:11,000
Tetris s'exécutant dans le coin, en multitâche, et puis tout d'un coup,

3
00:00:12,000 --> 00:00:14,000
plantage de KWin.

4
00:00:17,000 --> 00:00:22,000
Et l'audio ne s'est pas arrêté, Tetris continue dès qu'il s'en remet.

5
00:00:26,000 --> 00:00:29,000
Et notre environnement de développement (IDE) est toujours disponible.

6
00:00:29,000 --> 00:00:33,000
Et je peux l'exécuter plusieurs fois, simuler plusieurs plantages,

7
00:00:37,000 --> 00:00:39,000
et nous n'avons pas perdu une trame.

8
00:00:40,000 --> 00:00:45,000
Nous pouvons même copier du texte, copier ceci, redémarrer KWin.

9
00:00:46,000 --> 00:00:49,000
Et maintenant, c'est toujours sur mon plateau.

10
00:00:52,000 --> 00:00:53,000
Solide.
