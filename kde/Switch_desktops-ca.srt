1
00:00:00,000 --> 00:00:04,880
Ara comencem amb el Plasma de confiança.

2
00:00:04,880 --> 00:00:10,480
Tenim el CSGO obert, el «top» i aquesta petita aplicació personalitzada que he fet.

3
00:00:10,480 --> 00:00:13,720
Fem clic a un botó i ja estem al Weston.

4
00:00:13,720 --> 00:00:15,240
Totes les aplicacions han sobreviscut.

5
00:00:15,240 --> 00:00:17,040
Això no són capes addicionals.

6
00:00:17,040 --> 00:00:20,880
Són aquestes aplicacions que es mouen entre els compositors.

7
00:00:20,880 --> 00:00:23,120
Fem clic a un botó i torneu al Plasma.

8
00:00:23,120 --> 00:00:28,320
I curiosament hem anat des de la decoració del costat del client al costat del servidor i hem tornat.

9
00:00:28,320 --> 00:00:32,520
I moltes de les versions diferents i diferències s'estan gestionant implícitament dins del

10
00:00:32,520 --> 00:00:33,520
conjunt d'eines.

11
00:00:33,520 --> 00:00:36,800
Podem saltar al Gnome.

12
00:00:36,800 --> 00:00:38,760
Comencem en aquest efecte encantador de visió general.

13
00:00:38,760 --> 00:00:43,040
Tots els meus clients encara hi són, mantenint la seva mida.

14
00:00:43,040 --> 00:00:44,040
Puc anar al Hyprland.

15
00:00:44,040 --> 00:00:47,640
Esperem que es carregui.

16
00:00:47,640 --> 00:00:53,440
Aconseguim una animació molt bonica quan ho fa. Ei!

17
00:00:53,440 --> 00:00:56,120
Podem continuar movent-nos pel CSGO.

18
00:00:56,120 --> 00:00:57,760
Es comporta com abans.

19
00:00:57,760 --> 00:00:59,400
Tot funciona.

20
00:00:59,400 --> 00:01:00,400
Anem al Sway.

21
00:01:00,400 --> 00:01:04,959
Aquesta és l'única manera que vaig trobar com llançar una aplicació en el Sway.

22
00:01:04,959 --> 00:01:07,280
Les finestres encara hi són.
