1
00:00:00,000 --> 00:00:04,000
Ederki, aplikazio batzuk irekita daude, pelikula bat jotzen.

2
00:00:05,000 --> 00:00:11,000
Tetris-era jolasten izkinan, zeregin anitzetan, eta ondoren bat-batean,

3
00:00:12,000 --> 00:00:14,000
Gure KWin kraskatu egiten da.

4
00:00:17,000 --> 00:00:22,000
Eta audioa ez da gelditu, Tetris-ek jarraitzen du hura gainditzen duen bezain laster.

5
00:00:26,000 --> 00:00:29,000
Eta gure IDE oraindik erabilgarri dago.

6
00:00:29,000 --> 00:00:33,000
Eta hau behin eta berriro exekuta dezaket, kraskadura anizkoitzak simulatu,

7
00:00:37,000 --> 00:00:39,000
eta ez dugu fotograma bakarra galdu.

8
00:00:40,000 --> 00:00:45,000
Testu apur bat ere kopia dezakegu, kopiatu hori, KWin berrabiarazi.

9
00:00:46,000 --> 00:00:49,000
Eta orain, oraindik nire arbelean dago.

10
00:00:52,000 --> 00:00:53,000
Sendoa.
