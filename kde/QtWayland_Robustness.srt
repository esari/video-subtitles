1
00:00:00,000 --> 00:00:04,000
Okay, so we've got some applications go open, movie playing,

2
00:00:05,000 --> 00:00:11,000
Tetris playing in the corner, multi-tasking, and then all of a sudden,

3
00:00:12,000 --> 00:00:14,000
Our KWin crashes.

4
00:00:17,000 --> 00:00:22,000
And the audio didn't stop, Tetris is continuing as soon as it gets over it again.

5
00:00:26,000 --> 00:00:29,000
And our IDE is still available.

6
00:00:29,000 --> 00:00:33,000
And I can run this multiple times, simulate multiple crashes,

7
00:00:37,000 --> 00:00:39,000
and we didn't skip a frame.

8
00:00:40,000 --> 00:00:45,000
We can even copy some text, copy this, restart KWin.

9
00:00:46,000 --> 00:00:49,000
And now, it's still on my collect board.

10
00:00:52,000 --> 00:00:53,000
Robust.

