1
00:00:00,000 --> 00:00:04,880
Atëherë po ia fillojmë në Plasma-n e besueshme.

2
00:00:04,880 --> 00:00:10,480
Kemi të hapur CSGO, top-in dhe këtë aplikacion të vockël vetjak që kam krijuar.

3
00:00:10,480 --> 00:00:13,720
Klikoni një buton dhe buum, gjendemi në Weston.

4
00:00:13,720 --> 00:00:15,240
Krejt aplikacionet mbijetuan.

5
00:00:15,240 --> 00:00:17,040
Këtu s’ka shtresa shtesë.

6
00:00:17,040 --> 00:00:20,880
Bëhet fjalë për kalim të këtyre aplikacioneve nga një hartues në tjetër.

7
00:00:20,880 --> 00:00:23,120
Klikoni një buton dhe mbrapsht e te Plasma.

8
00:00:23,120 --> 00:00:28,320
Dhe, me interes, kemi kaluar nga zbukurime më anë klienti në ato më anë shërbyesi dhe mbrapsht.

9
00:00:28,320 --> 00:00:32,520
Dhe mjaft versione të ndryshëm dhe ndryshime po trajtohen pa u ndjerë brenda

10
00:00:32,520 --> 00:00:33,520
grupit të mjeteve.

11
00:00:33,520 --> 00:00:36,800
Mund të kalojmë nën Gnome.

12
00:00:36,800 --> 00:00:38,760
Fillojani me këtë efekt të ëmbël përmbledhës.

13
00:00:38,760 --> 00:00:43,040
Krejt klientët e mi janë ende atje, me madhësinë e tyre të ruajtur.

14
00:00:43,040 --> 00:00:44,040
Mund të kalohet në Hyprland.

15
00:00:44,040 --> 00:00:47,640
Pritni që të ngarkohet.

16
00:00:47,640 --> 00:00:53,440
Shihni një animacion shumë të hijshë, kur të ngarkohet. Uuuu!

17
00:00:53,440 --> 00:00:56,120
Mund të vazhdojmë të endemi nëpër CSGO.

18
00:00:56,120 --> 00:00:57,760
Sillet po si më parë.

19
00:00:57,760 --> 00:00:59,400
Gjithçka thjesht funksionon.

20
00:00:59,400 --> 00:01:00,400
Le të hidhemi në Sway.

21
00:01:00,400 --> 00:01:04,959
Kjo është e vetmja mënyrë që gjeta për të nisur një aplikacion në Sway.

22
00:01:04,959 --> 00:01:07,280
Dritaret janë ende atje.
