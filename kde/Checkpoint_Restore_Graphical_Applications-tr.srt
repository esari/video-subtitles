1
00:00:00,000 --> 00:00:08,000
Kullanıcı alanındaki Denetim Noktası Geri Yükleme, var olan bir uygulamayı çalışıyor olarak almanın ve durumunu diske kaydetmenin bir yoludur.

2
00:00:08,000 --> 00:00:14,520
Kulağa harika gibi geliyor; ancak grafik uygulamalarda işe yaramıyor, diyor web sitesinde,

3
00:00:14,520 --> 00:00:17,520
Hiçbir X11 uygulaması desteklenmez.

4
00:00:17,520 --> 00:00:24,520
Bileşikleştiriciler arasında geçiş yapmak için Wayland üzerinde yaptığımız çalışma bunu dolaylı olarak çözüyor.

5
00:00:25,000 --> 00:00:32,520
Bir demo yapacağız. RenkliBoya'yı açtım, Wayland logosuna yapıştırdığım bazı çizimler yapıyorum, güzel bir karışım hazırlıyorum.

6
00:00:33,520 --> 00:00:38,520
Güzel görünüyor ama 45 megabaytlık devasa bir RAM kullanıyor.

7
00:00:38,520 --> 00:00:45,520
Belki şimdi gidip WebKit'i derlemek gibi başka bir şey yapmam gerekiyor ve şimdi kullanılabilir olan her şeye gereksinimim var...

8
00:00:45,520 --> 00:00:47,000
...Megabayt.

9
00:00:47,000 --> 00:00:51,520
Bu yüzden criu dump komutunu çalıştırdım ve gitti. Diske kaydedildi.

10
00:00:52,000 --> 00:01:00,520
Yeniden başlatabilirim, çekirdek güncellemesi yapabilirim, hatta oluşturulan bu klasörü alıp başka bir dizüstü bilgisayara taşıyabilirim,

11
00:01:00,520 --> 00:01:04,519
ve ikili dosyaları orada olduğu sürece oradan geri yükleyebilirim.

12
00:01:04,519 --> 00:01:09,520
criu restore komutunu çalıştırırsam her şey anında geri gelir.

13
00:01:09,520 --> 00:01:15,520
Dolayısıyla bu mekanizma yavaş bir başlangıç sürecini paketlemek için de kullanılabilir.

14
00:01:16,520 --> 00:01:20,520
Ve logoyu oradan uzaklaştırabilirim, tam olarak bu duruma kaydedildiğini görebilirsiniz.

15
00:01:20,520 --> 00:01:27,520
Görseli diske kaydetseydim bu olanaklı olmazdı; çünkü RenkliBoya'da katman kavramı falan yoktur.

16
00:01:29,520 --> 00:01:32,520
Ve tam olarak olduğu yerde.

17
00:01:33,520 --> 00:01:44,520
Ancak geri yüklemede/kurtarmaktan daha fazlasını yapabiliriz... Eğer doğru yazabilirsem KDE'nin Mayın Tarlası klonu olan K Mayınlar'ı açıyorum.

18
00:01:45,520 --> 00:01:48,520
K Mayınlar'da pek iyi değilim, hamlemi yapıyorum.

19
00:01:48,520 --> 00:01:54,520
Bu kez onu dump yaptığımda, bu ek argümanı ekleyeceğim, çalışıyor olarak bırakacağım.

20
00:01:54,520 --> 00:01:59,520
Artık durumu kaydedip bir tahminde bulunabilirim.

21
00:01:59,520 --> 00:02:03,520
Eh, neyse bu pek iyi olmadı.

22
00:02:03,520 --> 00:02:12,520
Şimdi K Mayınlar'ı kapatırsam onu tam olarak bulunduğum yere geri yükleyebilirim ve bir kez daha deneyebilirim.

23
00:02:13,520 --> 00:02:18,520
Hâlâ yapamıyorum, neyse, kapatıp bir kez daha deneyebilirim.

24
00:02:18,520 --> 00:02:26,520
Belki Mayın Tarlası'nı oynamanın en iyi yolu bu olmayabilir; ancak hata ayıklama açısından pek yararlı oldu gibi görünüyor.

25
00:02:29,520 --> 00:02:41,520
Crio'nun kullanımı, bileşikleştirici aktarım çalışmasının geri kalanıyla aynı düzeyde değildir; ancak gelecekteki geliştiricilerin üzerine yapıp harika bir şeyler oluşturabilecekleri birçok seçeneğin kilidini açar.
