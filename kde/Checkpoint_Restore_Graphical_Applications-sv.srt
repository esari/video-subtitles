1
00:00:00,000 --> 00:00:08,000
Återställning av kontrollpunkt i användarområde är ett sätt att ta ett befintligt program som kör och spara dess tillstånd på disk.

2
00:00:08,000 --> 00:00:14,520
Det låter som att det kan vara fantastiskt, men från grafiska program fungerar det inte, står det på webbplatsen,

3
00:00:14,520 --> 00:00:17,520
inga X11-program stöds.

4
00:00:17,520 --> 00:00:24,520
Arbetet vi har gjort på Wayland för att byta mellan sammansättare löser det implicit.

5
00:00:25,000 --> 00:00:32,520
Vi gör en demo. Jag har KolourPaint öppet, jag håller på med en del konst, klistrade in en Wayland-logotyp, och gjorde en ljuvlig kombination.

6
00:00:33,520 --> 00:00:38,520
Och det ser snyggt ut, men det använder hela 45 megabyte RAM.

7
00:00:38,520 --> 00:00:45,520
Nu kanske jag behöver ta och göra något annat, som att kompilera WebKit, och nu behöver jag alla tillgängliga ...

8
00:00:45,520 --> 00:00:47,000
... megabyte.

9
00:00:47,000 --> 00:00:51,520
Så jag kör det här kommandot, criu dump, och det är borta. Sparat på disk.

10
00:00:52,000 --> 00:01:00,520
Jag kan starta om, jag kan göra en uppdatering av kärnan, jag kan till och med ta katalogen som det skapade, flytta den till en annan bärbar dator,

11
00:01:00,520 --> 00:01:04,519
och så länge binärfilerna finns där, återställa därifrån.

12
00:01:04,519 --> 00:01:09,520
Om jag kör criu restore kommer det tillbaka, och det kommer tillbaka direkt.

13
00:01:09,520 --> 00:01:15,520
Så mekanismen kan också användas för att köra igång en långsam startprocess.

14
00:01:16,520 --> 00:01:20,520
Och jag kan dra omkring logotypen, det syns att det exakt tillståndet är sparat.

15
00:01:20,520 --> 00:01:27,520
Det skulle inte vara möjligt om jag bara sparade bilden på disk, eftersom KolourPaint inte har något koncept med lager eller så.

16
00:01:29,520 --> 00:01:32,520
Och den är exakt där den fanns.

17
00:01:33,520 --> 00:01:44,520
Men vi kan göra mer än att bara spara från återställningen... Jag öppnar Minröjaren, en klon av Röja minor som vi har i KDE, om jag kan stava det rätt.

18
00:01:45,520 --> 00:01:48,520
Inte särskilt bra på Minröjaren, gör mitt drag.

19
00:01:48,520 --> 00:01:54,520
Men den här gången när jag dumpar den, ska jag lägga till ett extra argument, låta den köra.

20
00:01:54,520 --> 00:01:59,520
Och nu kan jag spara tillståndet, och gissa.

21
00:01:59,520 --> 00:02:03,520
Okej, inte en särskilt bra gissning, en besvikelse.

22
00:02:03,520 --> 00:02:12,520
Och nu, om jag stänger Minröjaren, kan jag återställa den, precis där jag var, och göra ett nytt försök.

23
00:02:13,520 --> 00:02:18,520
Äsch, fortfarande dålig på Minröjaren, jag kan stänga den, och fortsätta att försöka.

24
00:02:18,520 --> 00:02:26,520
Och det kanske inte det bästa sättet att spela Minröjaren, men det kan ha alla möjliga andra konsekvenser vid felsökning.

25
00:02:29,520 --> 00:02:41,520
Användningen av Crio är inte på samma nivå som resten av sammansättning överlämningsarbete, men det låser upp många alternativ för framtida utvecklare att bygga vidare på och göra något bra av.
