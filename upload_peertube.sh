#! /bin/sh
#
# SPDX-FileCopyrightText: 2023 Johnny Jazeix <jazeix@gmail.com>
#
# SPDX-License-Identifier: MIT

# Get the list of srt translated files that changed since last update
previousRevisionToCompare=`git rev-list -1 --before 2.days.ago origin/master`
allUpdatedFiles=`git diff --name-only HEAD ${previousRevisionToCompare} ":(exclude)po/" ":(exclude).reuse" | grep ".srt" | grep "-"`

if [ -z "$allUpdatedFiles" ]
then
    echo "Nothing to upload!"
    exit 0
else
    echo "Found these files to send: $allUpdatedFiles"
fi
# Upload all new/updated files on peertube
python3 ./upload_peertube.py -i $allUpdatedFiles $@
