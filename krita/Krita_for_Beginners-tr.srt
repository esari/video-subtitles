1
00:00:00,130 --> 00:00:02,040
Öncelikle herkese merhaba.

2
00:00:02,040 --> 00:00:07,140
Bu videonun sayısal resim yapmaya yeni başlayan ve öğrenmek için ilk seçenek olarak

3
00:00:07,330 --> 00:00:13,620
<b><font color=#ff0000>Krita</b></font>’yı seçen kişiler için olduğunu söylemek isterim. Yani orta düzey veya

4
00:00:14,139 --> 00:00:18,839
ileri düzey bir kullanıcıysanız büyük olasılıkla bu videodaki şeyleri <i>zaten biliyorsunuzdur</i>

5
00:00:18,840 --> 00:00:24,719
Bu videomuzda Krita’nın indirilmesi, sürücüler, Krita’nın düzgün kurulumu, karşılama ekranı,

6
00:00:24,789 --> 00:00:28,079
ilk görselimizi nasıl oluşturacağımız, fırçalar,

7
00:00:28,420 --> 00:00:35,790
çalışma alanı ve rengi, görsellerinizi nasıl kaydedip güvenli bir şekilde çalışacağınız hakkında konuşacağız. Peki hazır mısınız?

8
00:00:36,850 --> 00:00:38,850
Haydi başlayalım!

9
00:00:44,560 --> 00:00:47,940
İlk sayısal boyama programlarından bu yana her şey çok değişti.

10
00:00:48,550 --> 00:00:56,430
Bugünlerde heyecan verici zamanlarda yaşıyoruz; çünkü çevremiz yaratıcı uygulamalarla çevrili. Bugün her zamankinden daha fazla farklı

11
00:00:56,430 --> 00:00:59,159
araçla resim yapabiliyoruz ve bu harika.

12
00:00:59,160 --> 00:01:03,360
Ancak bazen elimizde o kadar fazla araç oluyor ki neyi kullanacağımızı şaşırıyoruz.

13
00:01:03,790 --> 00:01:11,069
Eğer buradaysanız Krita’ya ilgi duyduğunuz ve resim yapmaya, kendi dünyanızı yaratmaya başlamak istediğiniz içindir.

14
00:01:11,260 --> 00:01:16,259
Belki Krita ile yapılmış güzel bir görsel görmüşsünüzdür ve bu da

15
00:01:16,570 --> 00:01:19,410
Krita yazılımına olan merakınızı artırmaktadır.

16
00:01:19,660 --> 00:01:23,160
Bu video, Krita ile ilk deneyiminizi kolaylaştırmak için.

17
00:01:23,380 --> 00:01:28,499
İlk zamanlardan bu yana Krita, dikkatinizi dağıtacak veya korkutacak çok fazla düğmenin olmadığı,

18
00:01:28,780 --> 00:01:34,619
orta düzey bir arayüze sahip bir program olmuştur, bu her zaman takdir edilmiştir.

19
00:01:35,259 --> 00:01:38,489
Çünkü karmaşık bir arayüzü öğrenmek zorunda değiliz.

20
00:01:38,490 --> 00:01:44,580
Ancak bunu gereksinimlerimize uyacak şekilde ayarlayabiliriz. Eğer programı henüz kurmadıysanız bazı ipuçlarımız var.

21
00:01:44,619 --> 00:01:45,520
Öncelikle,

22
00:01:45,520 --> 00:01:47,520
Krita yazılımını nereden alabiliriz?

23
00:01:47,680 --> 00:01:53,610
Birçok kişi, birçok yazılımın sunulduğu sayfalarda yazılım arar.

24
00:01:53,800 --> 00:01:58,559
Kötü amaçlı yazılım veya casus yazılım gibi kötü sürprizlerle karşılaşabileceğimiz için bu önerilmez.

25
00:01:59,170 --> 00:02:01,170
Güvenli bir şekilde resim yapmak istiyorsanız

26
00:02:01,390 --> 00:02:08,970
Krita’yı indirmek için en iyi site resmi web sitesi krita.org’dur. Bağlantılar aşağıdaki açıklama kısmında olacaktır.

27
00:02:09,280 --> 00:02:13,830
İndirme düğmesini arayabilir veya çok açıklayıcı bir düğme olan “İndir” büyük düğmesine basabiliriz.

28
00:02:14,549 --> 00:02:20,909
Daha sonra Krita için istediğimiz platformu seçebiliriz.

29
00:02:21,010 --> 00:02:23,010
Yani örneğin

30
00:02:23,500 --> 00:02:28,139
Linux, Windows veya macOS kullanıyorsam. Krita’yı indirebiliriz ve

31
00:02:28,540 --> 00:02:35,249
indirirken burada ne olduğuna bir göz atalım. Gördüğünüz gibi Krita’yı uygulama olarak satın almak istersek

32
00:02:35,590 --> 00:02:39,479
Windows Mağazasına veya bir Steam platformuna da gidebiliriz.

33
00:02:39,480 --> 00:02:46,259
Ücretli içeriktir; ancak ana geliştiricimiz Boudewijn’in diğer geliştiricilere maaş ödemesine yardımcı oluyor, bu gerçekten önemli.

34
00:02:46,630 --> 00:02:51,540
Ayrıca gecelik yapılar denilen bir şeyin olduğunu da görebilirsiniz. Peki siz neyi tercih edersiniz?

35
00:02:52,380 --> 00:02:54,859
Kararlı sürüm mü yoksa geliştirme sürümü mü?

36
00:02:55,500 --> 00:02:58,130
Benim için en iyi Krita hangisi?

37
00:02:58,740 --> 00:03:00,380
Garip bir soru değil mi?

38
00:03:00,380 --> 00:03:00,870
Efendime söyleyeyim…

39
00:03:00,870 --> 00:03:03,740
Projenizi rahatça tamamlamak için kararlılığa ihtiyacınız varsa

40
00:03:04,140 --> 00:03:08,509
en yaygın seçenek kararlı sürümü indirmektir.

41
00:03:08,640 --> 00:03:12,830
İşinizi rahatlıkla bitirmek için temel özelliklere sahip olanıdır.

42
00:03:12,890 --> 00:03:20,330
Ancak en yeni özellikleri denemek ve hatta sonrasında geri bildirimde bulunmak istiyorsanız geliştirme sürümünü indirebilirsiniz.

43
00:03:21,030 --> 00:03:27,020
Ve evet, geliştirme sürümleri çökebilir ve evet bu iyi bir şeydir.

44
00:03:30,060 --> 00:03:36,229
Bu iyi bir şey; çünkü bu, programı iyileştirmemize ve Krita’nın bir sonraki sürüm için

45
00:03:36,360 --> 00:03:40,190
olabildiğince iyi olmasına izin veriyor ve iyi olan şu ki, gecelik yapıları indirebiliyorum

46
00:03:40,560 --> 00:03:46,970
ve kararlı sürümümü silmiyorum. Yani siz seçin. Krita’yı zaten indirdik.

47
00:03:47,120 --> 00:03:49,120
Şimdi sırada ne var? Sürücüler.

48
00:03:50,280 --> 00:03:56,179
Grafik tabletimiz için en son sürücüleri indirmek uygundur. Ekipmanımızı güncel tutmak

49
00:03:56,430 --> 00:04:02,810
her zaman iyi bir fikirdir ve hem Krita’da hem de diğer yazılımlarda birçok sorundan kurtulabiliriz.

50
00:04:03,299 --> 00:04:06,739
Bu yüzden her şeyi hazırlamak için biraz zaman ayırın.

51
00:04:06,739 --> 00:04:11,449
Sizi bekliyorum. Krita indirildikten ve sürücüleri güncelledikten sonra

52
00:04:11,450 --> 00:04:18,169
Krita’yı kurabilirsiniz. Tabletle ilgili sorunlar bulursanız tabletinizin Krita ile uyumlu olduğunu doğrulayın;

53
00:04:18,810 --> 00:04:24,649
Krita çok çeşitli aygıtları destekler; ancak bazen tuhaf davranışlarla karşılaşabiliriz.

54
00:04:24,690 --> 00:04:28,910
Bu bağlantıdan bakabilirsiniz. Ayrıca aşağıdaki açıklamada,

55
00:04:29,370 --> 00:04:35,419
bazı kullanıcılar Windows 10’da imlecin etrafındaki “halkalar” ile ilgili sorunlar bildirmişlerdir.

56
00:04:35,419 --> 00:04:38,299
Lütfen bu sorunun sizde olup olmadığına bakın.

57
00:04:39,120 --> 00:04:44,929
Krita’yı kurmak, indirdikten sonra karmaşık değildir, kurulum için dosyaya tıklayın.

58
00:04:45,810 --> 00:04:52,070
Ayrıca, Krita dosyalarını Windows’daki dosya tarayıcısızda görebilmek için Windows kabuğunun işaretlendiğinden emin olun.

59
00:04:52,919 --> 00:04:56,809
Linux kullanıcısıysanız bir Appimage indirebilirsiniz.

60
00:04:57,570 --> 00:05:00,830
İndirdikten sonra dosyayı yürütülebilir yapmanız gerekir.

61
00:05:01,740 --> 00:05:06,569
Appimage dosyanıza sağ tıklayın ve

62
00:05:06,879 --> 00:05:09,869
özellikleri seçip yürütülebilir olarak işaretleyin.

63
00:05:10,509 --> 00:05:16,619
Tamam, Krita’yı açın ve kaynakları yükleyen açılış ekranı belirir. Birkaç saniye sonra

64
00:05:16,620 --> 00:05:23,969
artık programın arayüzünü görebilirsiniz. Bir çeşit karşılama ekranımız var. Bu ekran birçok önemli bilgi verir.

65
00:05:24,610 --> 00:05:29,550
Örneğin Krita’nın en son haberlerini bu kutu sayesinde görebiliyoruz.

66
00:05:30,069 --> 00:05:32,968
Krita programının resmi hesabına bağlıdır.

67
00:05:33,400 --> 00:05:38,159
Ayrıca Krita dünyası ve topluluğu hakkında daha fazla bilgi edinmenize

68
00:05:38,319 --> 00:05:45,028
yardımcı olacak çok ilginç bağlantılar. Ayrıca doğrudan kılavuza gidebilir ve Krita geliştiricileri tarafından oluşturulan

69
00:05:45,159 --> 00:05:50,729
yararlı içerikleri okuyarak çok şey öğrenebiliriz. Doğrudan foruma gidebilir ve yardım arayabiliriz.

70
00:05:50,849 --> 00:05:55,199
Yalnızca kayıt olun, giriş yapın ve doğrudan geliştiricilerle konuşabilir

71
00:05:55,569 --> 00:06:00,929
fikirlerinizi paylaşabilir veya başka kullanıcılardan geri bildirim almak için sanat eserleri ekleyebilirsiniz.

72
00:06:01,210 --> 00:06:06,060
Programcılar ve sanatçılarla doğrudan iletişim. Güzel!

73
00:06:06,060 --> 00:06:10,259
Burada ilk sütunda “Kontrol+N” ortak kısayoluyla "Yeni Dosya”yı görüyoruz.

74
00:06:10,539 --> 00:06:15,869
Üzerine tıklarsam yeni bir pencere belirir ve birçok seçenek görürüm.

75
00:06:16,270 --> 00:06:22,259
Bir şablon seçebilir veya ihtiyacınız olan boyutta kendi dosyanızı oluşturabilirsiniz. Bir görsel oluşturmazsanız

76
00:06:22,479 --> 00:06:26,098
Krita’nın arayüzünün bazı kısımları çalışmaz; örneğin…

77
00:06:26,589 --> 00:06:31,199
Fırça seçimi, renk seçimi vb. Yalnızca menüler kullanılabilir.

78
00:06:31,629 --> 00:06:34,619
Bu mantıklıdır; çünkü üzerinde çalışılacak hiçbir şey yoktur.

79
00:06:34,719 --> 00:06:40,889
O halde ilk görselimizi Krita’da yapalım. “Özel Belge”ye gidin ve oluşturmak

80
00:06:40,889 --> 00:06:44,758
istediğiniz dosya boyutunu seçin ve tüm seçenekler konusunda endişelenmeyin.

81
00:06:44,759 --> 00:06:49,679
Başka bir videoda onlara da göz atacağız. Bir boyut verin ve “Oluştur” düğmesine tıklayın.

82
00:06:49,960 --> 00:06:53,279
Bir görsel oluşturduktan sonra üzerinde çalışmaya başlayabilirsiniz.

83
00:06:53,589 --> 00:06:58,919
Bunlar çok karmaşık ve bizi bunaltacak arayüzlerdir.

84
00:06:59,469 --> 00:07:01,469
Ben de bu yollardan geçtim.

85
00:07:01,569 --> 00:07:07,528
Peki resim yapmak için gerçekten neye gereksinimiz var? Fırça renklerine ve rahat

86
00:07:08,050 --> 00:07:09,189
bir çalışma

87
00:07:09,189 --> 00:07:11,549
alanına gereksinimimiz var; fırçalar, çalışma alanı ve renk.

88
00:07:12,250 --> 00:07:13,509
Fırçalar…

89
00:07:13,509 --> 00:07:19,809
Fırçalar nerede, bir şeyler nasıl boyanır, silinir, boyutları nasıl hızla değiştirilir ve bu kadar çok fırça

90
00:07:19,970 --> 00:07:23,470
seçeneği arasında nasıl kaybolmazsınız.

91
00:07:24,110 --> 00:07:28,330
Krita fırça kümesi herkesin temel gereksinimlerine uyacak şekilde tasarlanmıştır.

92
00:07:28,460 --> 00:07:29,289
Hepsinden önce

93
00:07:29,289 --> 00:07:32,229
Her şeyin doğru olduğundan emin olmak iyidir;

94
00:07:32,570 --> 00:07:40,329
çünkü yanlışlıkla “silgi modunu” etkinleştirdiğimizde bunu hatırlamayabiliriz ve aniden fırçalar boyamayabilir ve

95
00:07:40,639 --> 00:07:45,099
kafamız karışabilir. Bu nedenle

96
00:07:45,740 --> 00:07:48,820
silgi kipi düğmesinin kapalı olduğunu doğrulayın.

97
00:07:48,949 --> 00:07:53,348
Tamam! Fırçalar öntanımlı olarak arayüzün sağ tarafındadır.

98
00:07:53,690 --> 00:08:01,479
Ancak yalnızca paneli sürükleyerek konumlarını değiştirebilirsiniz. Fırçalar panelini kapatırsanız kolayca geri yükleyebilirsiniz.

99
00:08:01,759 --> 00:08:08,589
“Ayarlar → Paneller → Fırça Önayarları”na gittiğinizde Krita, gösterilecek son konumunu anımsar.

100
00:08:10,400 --> 00:08:12,519
Fırçalar birbirine çok benzeyebilir.

101
00:08:13,340 --> 00:08:17,440
Ama belki birkaç gün veya hafta içinde daha fazlasını isteyeceksiniz.

102
00:08:18,470 --> 00:08:25,209
Krita’nın mağazasında yağlıboya, pastel ve sulu boya günlerini veya haftalarını taklit edecek bir kümenin olduğunu söylemiş miydim?

103
00:08:27,500 --> 00:08:31,720
Krita, pek çok fırçanın kullanıcıyı bunaltabileceğini bilir.

104
00:08:32,180 --> 00:08:37,839
Böylece yalnızca belirli iş akışlarına yönelik bazı fırçaları görmek için kullanılabilecek

105
00:08:38,479 --> 00:08:40,479
bir etiket sistemi oluşturdu.

106
00:08:41,120 --> 00:08:45,039
Bunun nasıl çalıştığını görelim. Örneğin, “Boya”,

107
00:08:45,709 --> 00:08:47,029
“Eskiz”,

108
00:08:47,029 --> 00:08:54,789
“Doku” ve birçok başkası var. Her etikette o görevle ilgili birkaç fırça bulunur. Örneğin, “Dokular”

109
00:08:54,980 --> 00:09:02,380
etiketinde hızlı dokusal efektler oluşturmanıza yardımcı olacak bazı fırçalar bulunur. Gelecek videolarda, fırçalar hakkında daha fazla şey göreceğiz.

110
00:09:03,440 --> 00:09:08,020
Fırçaları aramanın başka bir yolu da süzmeyi kullanmaktır; örneğin fırça işletkeleri

111
00:09:08,570 --> 00:09:11,770
hakkında daha fazla bilgi edinmek için fırça düzenleyiciyi

112
00:09:12,230 --> 00:09:19,480
kullanarak fırçaları keşfetme konusunda daha derinlere gidebiliriz. Gelecek bir videoda bunu ele alacağız. Ama belki de fırçaların orada her zaman görünür

113
00:09:19,480 --> 00:09:23,980
sanki seni gözlemliyormuş gibi olmasından hoşlanmıyorsun. Bu durumda

114
00:09:24,350 --> 00:09:31,410
F6 klavye kısayolunu kullanabilir veya üstteki açılır pencereye giderek tüm fırçalarınızın bulunduğu pencereyi açabilirsiniz.

115
00:09:32,410 --> 00:09:37,439
Burada etiket sistemini kullanabilirsiniz. Başka ilginç bir seçeneğiniz daha vardır.

116
00:09:38,140 --> 00:09:41,009
Bazen 10’dan fazla fırça kullanmayız.

117
00:09:41,830 --> 00:09:46,770
Örneğin herhangi bir arayüz olmadan tam ekranda çizim yaptığımızı düşünün.

118
00:09:47,050 --> 00:09:54,540
“Sekme” kısayoluna bastıktan sonra sağ fare düğmesine veya sağ tıklamayla ilişkilendirdiğiniz kalem düğmesine basarsınız.

119
00:09:55,450 --> 00:10:00,150
Bu; fırçalar, renk, en son kullanılan renkler gibi şeyleri görmenizi sağlar.

120
00:10:00,150 --> 00:10:06,720
Ayrıca görseli yansıtın, döndürün ve daha fazlasını yapın. Bu hızlı menü gerçekten güçlüdür.

121
00:10:06,850 --> 00:10:12,119
Bunu başka bir videoda da göreceğiz. Bir fırça seçtiniz. Boyutu değiştirmek için

122
00:10:12,360 --> 00:10:20,320
“Üst Karakter” düğmesine basın ve boyutu artırmak için imleci sağa, küçültmek için sola sürükleyin.

123
00:10:20,520 --> 00:10:26,220
Bunu klavye kısayolları aracılığıyla da yapabiliriz.

124
00:10:26,920 --> 00:10:33,020
Fark edeceğiniz bir şey şu; örneğin

125
00:10:33,030 --> 00:10:35,309
bir fırça seçip boyutunu

126
00:10:35,890 --> 00:10:37,810
değiştirirseniz; bazı vuruşlar yaparsınız

127
00:10:37,810 --> 00:10:45,510
ve hımm bu çok güzel görünüyor çok iyi bir fırça diyorsun ve bu haliyle hoşuma gidiyor.

128
00:10:46,090 --> 00:10:50,639
Ancak başka bir fırça seçip bir önceki fırçayı tekrar seçerseniz

129
00:10:51,400 --> 00:10:55,800
fırçanın eski durumuna döndüğünü göreceksiniz.

130
00:10:56,740 --> 00:10:59,820
Bu durumda değişiklikleriniz çok fazla değildi.

131
00:11:00,370 --> 00:11:07,650
Ancak fırça parametreleriyle oynamaya başladığınızı ve sonunda tam olarak neyi değiştirdiğinizi bilmediğinizi;

132
00:11:07,650 --> 00:11:12,869
ancak fırça sonucunun çok harika olduğunu hayal edin. Bu durumda

133
00:11:12,880 --> 00:11:15,460
bu gerçekten can sıkıcı olabilir, değil mi?

134
00:11:15,580 --> 00:11:22,300
Bu, öntanımlı fırçaları olduğu gibi ve fazla değiştirmeden kullanmayı tercih eden kişiler için yapılır.

135
00:11:22,440 --> 00:11:29,240
Bu başlangıçta iyi bir uygulamadır; çünkü aralarındaki farkı ve tek bir fırçayla sahip olabileceğiniz

136
00:11:29,680 --> 00:11:35,310
olanakları daha iyi anlamanıza yardımcı olur ve Krita’nın “Kirli Fırçalar”ı

137
00:11:35,560 --> 00:11:42,859
yaratmasının nedeni de budur. Fırçaları tekrar tekrar değiştirseniz bile değişikliklerinizi hatırlayabilen fırçalar.

138
00:11:43,350 --> 00:11:45,890
Bu özelliği kullanmak için fırça düzenleyicisinde

139
00:11:46,410 --> 00:11:53,480
“İnce ayarları geçici olarak önayarlara kaydet” seçeneğini etkinleştirmeliyiz. Krita, kapanana kadar ayarları anımsayacaktır.

140
00:11:53,640 --> 00:11:58,849
Değiştirilmiş bir sürümü kaydetmek istiyorsanız üzerine yazma düğmesine tıklamanız yeterlidir;

141
00:11:59,040 --> 00:12:01,040
hepsi bu.

142
00:12:01,140 --> 00:12:04,969
Krita’yı kapatmış olsanız bile ilk modifiye edilmiş fırçanız

143
00:12:05,269 --> 00:12:11,479
kullanıma hazır. Rahat bir çalışma alanında olabilmek için temel dolaşımı nasıl yapacağımızı bilmemiz gerekiyor.

144
00:12:12,000 --> 00:12:17,779
Yakınlaştırma, kaydırma ve döndürme gibi özelliklerin yanı sıra yatay yansıtma da

145
00:12:18,029 --> 00:12:18,890
Krita’da faydalı olabilir.

146
00:12:18,890 --> 00:12:23,059
Görünümü yansıtmak için öntanımlı olarak “M” düğmesini kısayol olarak kullanırız.

147
00:12:23,370 --> 00:12:30,200
Uzun süre resim yaptığımızda bu gerçekten faydalıdır. Basit bir kısayol ama güçlü bir özellik.

148
00:12:31,110 --> 00:12:37,550
Renkli boyama yaparken renk seçebileceğimiz bir yer gerekiyor. Farklı renkleri tercih edebileceğiniz bir yer.

149
00:12:37,920 --> 00:12:43,490
Örneğin rengimi nasıl daha az doygun veya daha parlak hale getirebilirim? Örneğin,

150
00:12:44,190 --> 00:12:49,909
“Gelişmiş Renk Seçicisi”ni veya özel bir “Palet” kullanabiliriz. Şimdilik

151
00:12:49,950 --> 00:12:56,390
bunu öntanımlı olarak olduğu gibi bırakacağız çünkü önümüzdeki videolarda renklere değineceğiz.

152
00:12:57,240 --> 00:13:03,109
İşinizi kaydedin ve güvenli bir şekilde çalışın. Şiddetle tavsiye edilen bir uygulama, sık sık kaydetmektir.

153
00:13:03,570 --> 00:13:08,539
O yüzden lütfen yapın. Bazen tüm yazılımlar çöker. Ne diyelim artık…

154
00:13:08,540 --> 00:13:12,740
Sanırım öntanımlı olarak gelen fırçaları çizip sınıyorsunuz;

155
00:13:12,899 --> 00:13:17,659
ancak ya çok heyecanlanıp kaydetmeyi unutup bir anda ışıklar

156
00:13:18,000 --> 00:13:21,859
giderse ne olur? Yıllar önce bu büyük bir sorundu.

157
00:13:21,930 --> 00:13:27,770
Son kayıt zamanınızdan bu yana yaptığınız her şeyi yeniden yapmanıza neden olur.

158
00:13:28,740 --> 00:13:35,990
Ama teknolojik bir çağdayız değil mi? İşte bu yüzden Krita daha da ileri giderek bize nasıl yardım edecek?

159
00:13:36,750 --> 00:13:40,039
Bir görseli her açtığınızda ve onun üzerinde çalıştığınızda,

160
00:13:40,649 --> 00:13:42,889
Krita sessizce çalışmanızı kaydeder.

161
00:13:43,260 --> 00:13:50,179
Program dosyanın bir kopyasını oluşturur ve her 15 dakikada bir çalışmanızı kaydetmeye özen gösterir. ne kadar hoş değil mi?

162
00:13:51,059 --> 00:13:58,439
Bence öyle. Bu zamanı değiştirmek istersek “Ayarlar → Krita’yı Yapılandır → Genel → Dosya Kullanımı”

163
00:13:59,319 --> 00:14:02,218
bölümüne gidin ve zamanı değiştirin.

164
00:14:02,889 --> 00:14:04,719
Haydi yapalım.

165
00:14:04,719 --> 00:14:11,489
Garip bir nedenden dolayı beş dakikanın kayıt için uygun bir süre olduğunu düşünüyorum.

166
00:14:12,099 --> 00:14:18,959
Bu seçeneği devre dışı bırakmak istiyorsanız “Kendiliğinden Kaydetmeyi Etkinleştir” radyo düğmesine tıklamanız yeterlidir.

167
00:14:19,389 --> 00:14:22,108
Ben bunu yapmazdım. Bunu yapmayacağım.

168
00:14:22,109 --> 00:14:26,098
Bunu temin ederim. Çalışmamın farklı aşamalarını kaydetmek istersem ne olur?

169
00:14:26,529 --> 00:14:31,859
Daha sonra artımlı kaydetme seçeneğini çok kullanışlı bir özellik farklı adla kayıt için kullanabiliriz.

170
00:14:32,319 --> 00:14:34,348
Neden bu kadar faydalı olduğunu düşünüyorum?

171
00:14:34,719 --> 00:14:38,369
Çünkü kendisiyle ilişkilendirilmiş “Kontrol+Seçenek+S”

172
00:14:38,529 --> 00:14:44,669
gibi bir kısayolla üzerinde çalıştığımız dosyanın artımlı bir sürümünü oluşturabiliriz.

173
00:14:44,949 --> 00:14:51,478
Verimliliğe yardımcı olan bu tür araçları seviyorsanız yorumlarda bana söyleyin

174
00:14:51,669 --> 00:14:56,759
ve Krita’yı beğenin. Tamam! Şimdilik bu kadar!

175
00:14:56,759 --> 00:14:59,878
Artık temel bilgilere sahipsiniz; ancak çok daha fazlası var.

176
00:15:00,189 --> 00:15:03,898
Abone olmayı ve bu videoyu sosyal medyanızda paylaşmayı unutmayın!

177
00:15:03,899 --> 00:15:09,719
Bunun arkadaşlarınıza ve daha fazla Krita kullanıcısına yardımcı olabileceğini düşünüyorsanız paylaşın.

178
00:15:10,179 --> 00:15:15,119
Belirli bir konuyla ilgileniyorsanız veya yeni konular önermek istiyorsanız

179
00:15:15,879 --> 00:15:21,028
aşağıda yorumlarda bana bildirin, belki bir sonraki video bu olabilir.

180
00:15:21,969 --> 00:15:28,169
İzlediğiniz için çok teşekkürler. Krita’nın daha gelişmiş özelliklerle nasıl kullanılacağını

181
00:15:28,239 --> 00:15:30,239
anlatacağım yeni videolara hazır olun.

182
00:15:30,789 --> 00:15:32,789
Görüşürüz!
