1
00:00:00,130 --> 00:00:02,040
Najprej lep pozdrav vsem.

2
00:00:02,040 --> 00:00:07,140
Ta posnetek je namenjen tistim, ki se spoznavajo z digitalnim slikanjem in

3
00:00:07,330 --> 00:00:13,620
so izbrali <b><font color=#ff0000>Krito</b></font> kot prvo možnost učenja. Torej, če ste povprečni ali napredni uporabnik,

4
00:00:14,139 --> 00:00:18,839
<i>boste verjetno v tem videu videli stvari, ki jih že poznate.</i>

5
00:00:18,840 --> 00:00:24,719
Govorili bomo o: prenosu Krite, gonilnikih, pravilnem nameščanju Krite,

6
00:00:24,789 --> 00:00:28,079
zaslonu dobrodošlice, kako ustvariti prvo sliko,

7
00:00:28,420 --> 00:00:35,790
o čopičih, delovni površini in barvah, kako shraniti slike in delati varno. Ali ste pripravljeni?

8
00:00:36,850 --> 00:00:38,850
Pa začnimo.

9
00:00:44,560 --> 00:00:47,940
Od pojava prvih programov za digitalno slikanje se je vse zelo spremenilo.

10
00:00:48,550 --> 00:00:56,430
Živimo v razburljivih časih, saj smo obkroženi s programi za ustvarjanje. Danes lahko bolj kot kadarkoli

11
00:00:56,430 --> 00:00:59,159
slikamo z veliko različnimi orodji - in to je odlično.

12
00:00:59,160 --> 00:01:03,360
Toda včasih smo zasičeni z obilico možnosti, ki jih imamo na voljo.

13
00:01:03,790 --> 00:01:11,069
Če ste tukaj, je to zato, ker vas zanima Krita in želite začeti slikati in ustvarjati svoj svet.

14
00:01:11,260 --> 00:01:16,259
Morda ste videli čudovito sliko, narejeno s Krito in to

15
00:01:16,570 --> 00:01:19,410
stopnjuje vašo radovednost glede programja Krita.

16
00:01:19,660 --> 00:01:23,160
No, ta videoposnetek vam bo olajšal prve

17
00:01:23,380 --> 00:01:28,499
izkušnje s Krito. Od svojih začetkov je Krita program s srednje zahtevnim

18
00:01:28,780 --> 00:01:34,619
vmesnikom brez preveč gumbov, ki bi vas motili ali ustrahovali, kar je vedno dobrodošlo,

19
00:01:35,259 --> 00:01:38,489
saj se nam ni treba naučiti kompleksnega vmesnika.

20
00:01:38,490 --> 00:01:44,580
Lahko pa ga prilagodimo svojim potrebam. Če še niste namestili programa, sledi nekaj nasvetov.

21
00:01:44,619 --> 00:01:45,520
Najprej:

22
00:01:45,520 --> 00:01:47,520
Kje dobimo programsko opremo Krita?

23
00:01:47,680 --> 00:01:53,610
Veliko ljudi išče programsko opremo na straneh, ki ponujajo veliko programske opreme. To ni priporočljivo,

24
00:01:53,800 --> 00:01:58,559
ker lahko najdemo podtaknjence, kot je zlonamerna ali vohunska programska oprema.

25
00:01:59,170 --> 00:02:01,170
Torej, če želite barvati varno,

26
00:02:01,390 --> 00:02:08,970
je najboljša stran za prenos Krite uradna spletna stran Krita.org. Povezave bodo v

27
00:02:09,280 --> 00:02:13,830
opisu spodaj. Lahko poiščemo gumb za prenos ali samo pritisnemo velik gumb

28
00:02:14,549 --> 00:02:20,909
»Get Krita now (Dobi Krito zdaj)«, zelo opisen gumb. Nato lahko izberemo platformo, za katero želimo Krito.

29
00:02:21,010 --> 00:02:23,010
Glede na to, ali uporabljamo

30
00:02:23,500 --> 00:02:28,139
Linux, Windows ali macOS. Krito lahko prenesemo in medtem, ko se prenaša,

31
00:02:28,540 --> 00:02:35,249
poglejmo, kaj imamo tukaj. Kot lahko vidite, lahko obiščemo tudi trgovino Windows Store ali platformo Steam,

32
00:02:35,590 --> 00:02:39,479
če želimo dobiti Krito kot aplikacijo. To je plačljiva vsebina,

33
00:02:39,480 --> 00:02:46,259
vendar pomaga plačevati glavnega razvijalca Krite Boudewijna, kar je res pomembno. Vidite lahko tudi, da obstaja nekaj, kar se imenuje

34
00:02:46,630 --> 00:02:51,540
nočne gradnje. Krita ima dve različni različici. Kaj imate torej raje?

35
00:02:52,380 --> 00:02:54,859
stabilno različico ali razvojno različico?

36
00:02:55,500 --> 00:02:58,130
Katera Krita je zame najboljša?

37
00:02:58,740 --> 00:03:00,380
Čudno vprašanje, kajne?

38
00:03:00,380 --> 00:03:00,870
No ...

39
00:03:00,870 --> 00:03:03,740
Če potrebujete stabilnost za dokončanje projekta,

40
00:03:04,140 --> 00:03:08,509
je najbolj običajna izbira prenos stabilne različice.

41
00:03:08,640 --> 00:03:12,830
Je tista, ki ima glavne funkcije za dokončanje dela.

42
00:03:12,890 --> 00:03:20,330
Če pa želite preizkusiti najnovejše funkcije in po tem celo podati povratne informacije, lahko prenesete

43
00:03:21,030 --> 00:03:27,020
razvojno različico. Res je, razvojna izdaja se lahko sesuje, in da, to je dobro.

44
00:03:30,060 --> 00:03:36,229
Dobro je, ker nam to omogoča, da nadaljujemo z razvojem in kar se da izbrusimo Krito

45
00:03:36,360 --> 00:03:40,190
za naslednjo izdajo in dobra stvar je, da lahko prenesemo

46
00:03:40,560 --> 00:03:46,970
nočne gradnje in ne izbrišemo svoje stabilne izdaje. Torej izberite. Krito smo že prenesli,

47
00:03:47,120 --> 00:03:49,120
kaj pa zdaj? Gonilniki.

48
00:03:50,280 --> 00:03:56,179
Priročno je prenesti najnovejše gonilnike za svojo grafično tablico. Imeti svojo opremo

49
00:03:56,430 --> 00:04:02,810
posodobljeno je vedno dobro in lahko rešimo marsikatero težavo tako s Krito kot z drugimi programi.

50
00:04:03,299 --> 00:04:06,739
Zato vložite nekaj časa v pripravo vsega skupaj.

51
00:04:06,739 --> 00:04:11,449
Počakal bom. Ko je Krita prenesena in ste posodobili gonilnike,

52
00:04:11,450 --> 00:04:18,169
lahko namestite Krito. Če odkrijete težave z grafično tablico, preverite, ali je združljiva s Krito.

53
00:04:18,810 --> 00:04:24,649
Krita pokriva široko paleto naprav, vendar včasih lahko odkrijemo nenavadno vedenje.

54
00:04:24,690 --> 00:04:28,910
Tukaj je povezava, da to preverite. Tudi v spodnjem opisu.

55
00:04:29,370 --> 00:04:35,419
Nekateri uporabniki so poročali o težavah z »obročki« okoli kazalca v sistemu Windows 10.

56
00:04:35,419 --> 00:04:38,299
Če imate takšno težavo, si oglejte ta videoposnetek.

57
00:04:39,120 --> 00:04:44,929
Namestitev Krite ni zapletena; ko jo prenesete, kliknite datoteko za namestitev.

58
00:04:45,810 --> 00:04:52,070
Prepričajte se tudi, da je lupina Windows označena, da lahko vidite datoteke krita v brskalniku datotek v sistemu Windows.

59
00:04:52,919 --> 00:04:56,809
Če uporabljate Linux, lahko prenesete sliko aplikacije.

60
00:04:57,570 --> 00:05:00,830
Ko je prenesena, morate datoteko narediti izvršljivo,

61
00:05:01,740 --> 00:05:06,569
tako da potrdite nadzorna dovoljenja. Z desno tipko miške kliknite slikovno datoteko aplikacije in

62
00:05:06,879 --> 00:05:09,869
izberite lastnosti in jo nato naredite izvršljivo.

63
00:05:10,509 --> 00:05:16,619
V redu, odprite Krito in prikaže se začetni zaslon, ki nalaga vire. Po nekaj sekundah

64
00:05:16,620 --> 00:05:23,969
lahko vidite vmesnik programa. Imamo nekakšen pozdravni zaslon. Ta zaslon ponuja veliko pomembnih informacij.

65
00:05:24,610 --> 00:05:29,550
Vidimo lahko npr. zadnje novice o Kriti. Zahvaljujoč temu polju,

66
00:05:30,069 --> 00:05:32,968
povezanemu z uradnim računom Krita painting.

67
00:05:33,400 --> 00:05:38,159
In tudi zelo zanimive povezave, ki vam pomagajo izvedeti več o

68
00:05:38,319 --> 00:05:45,028
svetu Krite in njegove skupnosti. Prav tako lahko gremo neposredno na priročnik in se veliko naučimo z branjem

69
00:05:45,159 --> 00:05:50,729
dobrih vsebin, ki so jih ustvarili razvijalci Krite. Lahko gremo neposredno na forum in poiščemo pomoč.

70
00:05:50,849 --> 00:05:55,199
Registrirajte račun, se prijavite in že ste v neposrednem stiku z

71
00:05:55,569 --> 00:06:00,929
razvijalci in delite svoje ideje ali dodate umetnine, da prejmete odziv drugih uporabnikov.

72
00:06:01,210 --> 00:06:06,060
Neposreden stik s programerji in umetniki. Krasno! Tukaj v prvem stolpcu

73
00:06:06,060 --> 00:06:10,259
vidimo »novo datoteko« z običajno bližnjico »krmilka+N«.

74
00:06:10,539 --> 00:06:15,869
Če kliknem nanjo, se prikaže novo okno in vidim veliko možnosti.

75
00:06:16,270 --> 00:06:22,259
Izberete lahko predlogo ali ustvarite lastno datoteko želene velikosti. Če ne ustvarite slike,

76
00:06:22,479 --> 00:06:26,098
nekateri deli Kritinega vmesnika ne delujejo, kot npr. ...

77
00:06:26,589 --> 00:06:31,199
Izbor čopiča, izbor barve itn. Na voljo so samo meniji.

78
00:06:31,629 --> 00:06:34,619
To je logično, ker ni s čim delati.

79
00:06:34,719 --> 00:06:40,889
Ustvarimo prvo sliko v Kriti. Pojdite na dokument po meri in izberite velikost datoteke,

80
00:06:40,889 --> 00:06:44,758
ki jo želite ustvariti, in naj vas vse mogoče možnosti ne prestrašijo.

81
00:06:44,759 --> 00:06:49,679
Ogledali si jih bomo v drugem videu. Podajte velikost in kliknite gumb Ustvari.

82
00:06:49,960 --> 00:06:53,279
Ko ustvarite sliko, lahko začnete delati na njej.

83
00:06:53,589 --> 00:06:58,919
Obstajajo vmesniki, ki so zelo kompleksni in nas preobremenijo.

84
00:06:59,469 --> 00:07:01,469
Tudi meni se je to že zgodilo.

85
00:07:01,569 --> 00:07:07,528
Toda kaj pravzaprav potrebujemo za slikanje? Potrebujemo čopiče, barve in udobno

86
00:07:08,050 --> 00:07:09,189
delovno površino.

87
00:07:09,189 --> 00:07:11,549
Čopiči, delovna površina in barva

88
00:07:12,250 --> 00:07:13,509
Torej, čopiči ...

89
00:07:13,509 --> 00:07:19,809
Kje so čopiči in kako slikati in brisati stvari, kako hitro spremeniti njihovo velikost in

90
00:07:19,970 --> 00:07:23,470
kako se ne izgubiti med toliko možnostmi čopičev?

91
00:07:24,110 --> 00:07:28,330
Komplet čopičev Krita je zasnovan tako, da ustreza slehernim osnovnim potrebam.

92
00:07:28,460 --> 00:07:29,289
Najprej

93
00:07:29,289 --> 00:07:32,229
se je dobro prepričati, da je vse pravilno,

94
00:07:32,570 --> 00:07:40,329
saj se lahko zgodi, da pomotoma aktiviramo »način radirke« in se tega ne spomnimo in nenadoma

95
00:07:40,639 --> 00:07:45,099
čopiči ne slikajo in to nas lahko zmede. Torej

96
00:07:45,740 --> 00:07:48,820
preverite, ali je gumb za način radirke izklopljen.

97
00:07:48,949 --> 00:07:53,348
V redu! Čopiči so privzeto na desni strani vmesnika.

98
00:07:53,690 --> 00:08:01,479
Lahko pa spremenite, kje se nahajajo, tako da preprosto povlečete zloženo okno. Če zaprete zloženo okno čopičev, ga lahko preprosto obnovite.

99
00:08:01,759 --> 00:08:08,589
Odprite prednastavitev nastavitve/čopiča in Krita si zapomni mesto svojega zadnjega prikaza.

100
00:08:10,400 --> 00:08:12,519
Čopiči so si morda na videz zelo podobni,

101
00:08:13,340 --> 00:08:17,440
vendar pa si jih boste čez nekaj dni ali tednov morda želeli še več.

102
00:08:18,470 --> 00:08:25,209
Ali sem rekel, da je v Kritini trgovini cel komplet za posnemanje oljnih, pastelnih in akvarelskih?

103
00:08:27,500 --> 00:08:31,720
Krita ve, da lahko obilica čopičev preobremeni uporabnika.

104
00:08:32,180 --> 00:08:37,839
Tako je ustvarila sistem oznak, ki jih je mogoče uporabiti za ogled le nekaterih čopičev

105
00:08:38,479 --> 00:08:40,479
glede na določene poteke dela.

106
00:08:41,120 --> 00:08:45,039
Poglejmo, kako to deluje. Primer: imamo »Sliko«,

107
00:08:45,709 --> 00:08:47,029
»Skico«,

108
00:08:47,029 --> 00:08:54,789
»Teksturo« in tako naprej. Vsaka oznaka ima nekaj čopičev, povezanih s to nalogo. Primer: oznaka »Tekstura«

109
00:08:54,980 --> 00:09:02,380
Ima nekaj čopičev, ki vam pomagajo ustvariti hitre teksturne učinke. V prihodnjih videih boste izvedeli več o čopičih.

110
00:09:03,440 --> 00:09:08,020
Drug način iskanja čopičev je uporaba filtriranja, lahko gremo globlje v

111
00:09:08,570 --> 00:09:11,770
raziskovanje čopičev z urejevalnikom čopičev, na primer,

112
00:09:12,230 --> 00:09:19,480
če želite izvedeti več o pogonih čopičev. To bomo obravnavali v naslednjem videu. Morda pa ne marate imeti čopičev

113
00:09:19,480 --> 00:09:23,980
tam, vedno vidnih, kot bi vas opazovali. V tem primeru lahko uporabite

114
00:09:24,350 --> 00:09:31,410
bližnjico na tipkovnici F6 ali obiščete spustno okno na vrhu, ki bo odprlo okno z vsemi vašimi čopiči.

115
00:09:32,410 --> 00:09:37,439
Tukaj lahko uporabite sistem oznak. Še vedno imate še eno zelo zanimivo možnost.

116
00:09:38,140 --> 00:09:41,009
Včasih ne uporabljamo več kot 10 čopičev.

117
00:09:41,830 --> 00:09:46,770
Predstavljajte si, da rišemo na celotnem zaslonu brez vmesnika,

118
00:09:47,050 --> 00:09:54,540
pritisnite »tabulatorko«, nato pritisnite desni gumb miške ali gumb pisala, ki ste ga povezali z desnim klikom.

119
00:09:55,450 --> 00:10:00,150
To vam omogoča ogled stvari, kot so čopiči, barve, zadnje uporabljene barve.

120
00:10:00,150 --> 00:10:06,720
Prav tako lahko zrcalite sliko, zavrtite sliko in drugo. Ta hitri meni je res uporaben.

121
00:10:06,850 --> 00:10:12,119
To bomo videli tudi v drugem videu. Izbrali ste čopič. Za spremembo velikosti

122
00:10:12,360 --> 00:10:20,320
pritisnite »dvigalko« in povlecite kazalec v desno za povečanje velikosti ali v levo za zmanjšanje velikosti.

123
00:10:20,520 --> 00:10:26,220
To lahko storimo tudi prek bližnjic na tipkovnici. Pa ni nič udobnega oz.

124
00:10:26,920 --> 00:10:33,020
skozi drsnik na vrhu vmesnika, kar je dobra ideja. Ena stvar, ki jo boste opazili,

125
00:10:33,030 --> 00:10:35,309
je, da če izberete čopič in

126
00:10:35,890 --> 00:10:37,810
spremenite npr. velikost,

127
00:10:37,810 --> 00:10:45,510
naredite nekaj potez in rečete, hmm, tale izgleda zelo dobro, zelo dober čopič in všeč mi je tak, kot je.

128
00:10:46,090 --> 00:10:50,639
Vendar če izberete drug čopič in ponovno izberete prejšnji čopič

129
00:10:51,400 --> 00:10:55,800
boste videli, da se je čopič vrnil v prejšnje stanje.

130
00:10:56,740 --> 00:10:59,820
V tem primeru vaše spremembe niso bile prevelike.

131
00:11:00,370 --> 00:11:07,650
Toda predstavljajte si, da se začnete igrati s parametri čopiča in na koncu ne veste, kaj točno ste spremenili,

132
00:11:07,650 --> 00:11:12,869
ampak rezultat čopiča je zelo kul. V tem primeru

133
00:11:12,880 --> 00:11:15,460
je to lahko zelo nadležno, kajne?

134
00:11:15,580 --> 00:11:22,300
To je namenjeno ljudem, ki raje uporabljajo privzete čopiče takšne, kot so, in jih ne spreminjajo preveč.

135
00:11:22,440 --> 00:11:29,240
To je dobra praksa na začetku, saj vam pomaga bolje razumeti razliko med njimi in

136
00:11:29,680 --> 00:11:35,310
možnostmi, ki jih lahko imate z enim čopičem, in to je razlog, zakaj je Krita ustvarila

137
00:11:35,560 --> 00:11:42,859
»Umazane čopiče«. Čopiče, ki si lahko zapomnijo vaše spremembe, tudi če vedno znova menjate čopiče.

138
00:11:43,350 --> 00:11:45,890
Za uporabo te funkcije moramo aktivirati

139
00:11:46,410 --> 00:11:53,480
»Začasno shrani prilagoditve med prednastavitve« v urejevalniku čopičev, da Krita pomni nastavitve, dokler se ne zapre.

140
00:11:53,640 --> 00:11:58,849
V redu, če želite shraniti spremenjeno različico, preprosto kliknite gumb za prepis

141
00:11:59,040 --> 00:12:01,040
in to je to.

142
00:12:01,140 --> 00:12:04,969
Svoj prvi spremenjeni čopič imate pripravljen za uporabo,

143
00:12:05,269 --> 00:12:11,479
tudi če zaprete Krito. Da bi imeli udobno delovno površino, moramo poznati osnovno krmarjenje,

144
00:12:12,000 --> 00:12:17,779
kot je povečava, premikanje, vrtenje in tudi vodoravno zrcaljenje so lahko uporabni.

145
00:12:18,029 --> 00:12:18,890
V Kriti

146
00:12:18,890 --> 00:12:23,059
privzeto uporabljamo tipko »M« kot bližnjico za zrcaljenje pogleda.

147
00:12:23,370 --> 00:12:30,200
To je zelo koristno, ko slikamo dlje časa. Preprosta bližnjica, a zmogljiva funkcija. Zdaj pa barva.

148
00:12:31,110 --> 00:12:37,550
Ko slikamo, moramo umestiti okno, iz katerega lahko izbiramo barve. Mesto, kjer lahko izbirate različne barve.

149
00:12:37,920 --> 00:12:43,490
Kako npr. naredimo barvo manj nasičeno ali svetlejšo? Primer:

150
00:12:44,190 --> 00:12:49,909
uporabimo lahko »napredni izbirnik barv« ali pa »paleto« po meri. Za zdaj

151
00:12:49,950 --> 00:12:56,390
bomo privzeto pustili tako, kot je, ker se bomo z barvami ukvarjali v naslednjih videoposnetkih.

152
00:12:57,240 --> 00:13:03,109
Shranjevanje slik in varno delo. Zelo priporočljiva praksa je pogosto shranjevanje.

153
00:13:03,570 --> 00:13:08,539
Zato, prosim, storite to. Vsaka programska oprema se kdaj sesuje. No, …

154
00:13:08,540 --> 00:13:12,740
Predvidevam, da ste risali in preizkušali čopiče, ki so bili privzeti,

155
00:13:12,899 --> 00:13:17,659
a kaj se zgodi, če smo tako navdušeni, da pozabimo shraniti in

156
00:13:18,000 --> 00:13:21,859
nenadoma ugasne luč? Pred leti je bil to velik problem.

157
00:13:21,930 --> 00:13:27,770
Zaradi tega morate znova ponoviti vse od zadnjega shranjevanja svojega dela.

158
00:13:28,740 --> 00:13:35,990
Ampak smo v tehnološki dobi, kajne? Zato gre Krita dlje in nam pomaga. Kako?

159
00:13:36,750 --> 00:13:40,039
Vsakič, ko odprete sliko in delate na njej,

160
00:13:40,649 --> 00:13:42,889
Krita tiho shranjuje vaše delo.

161
00:13:43,260 --> 00:13:50,179
Program vsakih 15 minut naredi kopijo datoteke in poskrbi za shranjevanje dela. Ali ni to kul?

162
00:13:51,059 --> 00:13:58,439
Mislim, da je. Če želimo spremeniti ta čas, moramo iti v meni nastavitev/Prilagodi Krito/Splošno/razdelek in

163
00:13:59,319 --> 00:14:02,218
/Ravnanje z datotekami in tam je sprememba časa.

164
00:14:02,889 --> 00:14:04,719
Naredimo to.

165
00:14:04,719 --> 00:14:11,489
Menim, da je pet minut nemoteč čas za shranjevanje, če gre vse iz neznanega razloga po gobe.

166
00:14:12,099 --> 00:14:18,959
Če želite deaktivirati to možnost, preprosto kliknite izbirni gumb »Omogoči samodejno shranjevanje«.

167
00:14:19,389 --> 00:14:22,108
Jaz tega ne bi naredil. Ne bom tega naredil.

168
00:14:22,109 --> 00:14:26,098
To lahko zagotovim. Kaj pa, če želim shraniti različne faze svojega dela?

169
00:14:26,529 --> 00:14:31,859
V tem primeru uporabimo možnost postopnega shranjevanja. Kul ime za zelo uporabno funkcijo.

170
00:14:32,319 --> 00:14:34,348
In zakaj mislim, da je tako koristna?

171
00:14:34,719 --> 00:14:38,369
Ker če opazite, da ima povezano bližnjico,

172
00:14:38,529 --> 00:14:44,669
kar je »krmilka+izmenjalka+S«, in na ta način samo pritisnemo bližnjico in ustvarimo

173
00:14:44,949 --> 00:14:51,478
Inkrementalno različico datoteke, na kateri delamo. Če vam je všeč ta vrsta orodij, ki pomagajo pri produktivnosti,

174
00:14:51,669 --> 00:14:56,759
mi sporočite v komentarjih in všečkajte Krito. V redu! To je zaenkrat konec!

175
00:14:56,759 --> 00:14:59,878
Spoznali ste osnove, vendar je še veliko, veliko več.

176
00:15:00,189 --> 00:15:03,898
Ne pozabite se naročiti in deliti ta videoposnetek na svojih družbenih omrežjih.

177
00:15:03,899 --> 00:15:09,719
Če mislite, da bi to lahko pomagalo vašim prijateljem in drugim uporabnikom Krite, potem to delite.

178
00:15:10,179 --> 00:15:15,119
Če vas zanima določena tema ali želite predlagati nove teme,

179
00:15:15,879 --> 00:15:21,028
mi sporočite v spodnjih komentarjih in morda bo to naslednji videoposnetek.

180
00:15:21,969 --> 00:15:28,169
Najlepša hvala za ogled in bodite pripravljeni na nove videoposnetke, v katerih bomo pogledali, kako uporabljati Krito

181
00:15:28,239 --> 00:15:30,239
z naprednejšimi zmožnostmi.

182
00:15:30,789 --> 00:15:32,789
Adijo.
