﻿1
00:00:01,120 --> 00:00:04,520
Hello everybody! and welcome to this new video!

2
00:00:04,520 --> 00:00:08,200
We are celebrating the 25th anniversary of Krita

3
00:00:08,200 --> 00:00:13,960
and today! Here! we have a very, very, special guest.

4
00:00:14,000 --> 00:00:16,600
The Krita´s mantainer...Halla Rempt

5
00:00:16,600 --> 00:00:21,600
And Halla has been the mantainer during more that 20 years!

6
00:00:21,600 --> 00:00:22,960
Is Amazing!

7
00:00:22,960 --> 00:00:25,880
a very special day, enjoy  it!

8
00:00:25,880 --> 00:00:28,120
Hi Halla, how you doing?

9
00:00:28,400 --> 00:00:30,200
Hi Ramon, nice to see you!

10
00:00:30,440 --> 00:00:34,960
Celebrating 25 years is a significant milestone.

11
00:00:34,960 --> 00:00:39,200
What being open source meant for the development of Krita?

12
00:00:39,800 --> 00:00:45,120
What being open source meant for the development of Krita in the past 25 years? Well, I would say

13
00:00:45,120 --> 00:00:50,520
it's absolutely essential and that's for two reasons. First of course is that being open source

14
00:00:50,520 --> 00:00:55,680
have drawn on the contributions  of hundreds of people. There are over 600

15
00:00:55,680 --> 00:01:00,440
people in Krita´s commit list and that's just the development. Not the websites,

16
00:01:00,440 --> 00:01:05,120
not the documentation, not everything else and that's awesome! Whenever we got stuck, somewhere,

17
00:01:05,120 --> 00:01:10,720
people could help us! and lots of people did. Finally perhaps even more importantly, now that

18
00:01:10,720 --> 00:01:17,200
Krita is open source free software under the GPL, it can never ever be closed. When over the

19
00:01:17,200 --> 00:01:22,800
years we've seen competitors disappear because they were bought out by companies like Adobe,

20
00:01:22,800 --> 00:01:27,440
we just knew we could trust in that Krita would just continue as long as there were

21
00:01:27,440 --> 00:01:32,680
contributors. So being open source is totally fundamental, that's  never going to change!

22
00:01:32,760 --> 00:01:35,240
Wow! this is very interesting info.

23
00:01:35,240 --> 00:01:40,240
Krita has been during these 25 years around,

24
00:01:40,240 --> 00:01:42,200
which is an incredible journey.

25
00:01:42,200 --> 00:01:46,160
How Krita evolved during these 25 years?

26
00:01:46,200 --> 00:01:50,240
25 years that's quite a long journey... I haven't been there for all of that  Journey.

27
00:01:50,240 --> 00:01:54,840
Maybe only 21 years which is still a long time. When I look back at

28
00:01:54,840 --> 00:01:58,960
what Krita looked like when I joined the project, you couldn't even draw.

29
00:01:58,960 --> 00:02:02,720
You could load images add layers and move layers around and that was it.

30
00:02:02,720 --> 00:02:08,320
So, we started building on that. We rewrote the entire core of Krita. Core has been rewritten 4 times

31
00:02:08,320 --> 00:02:15,680
and every time Krita became more performant we started adding tools, we started adding phases

32
00:02:15,680 --> 00:02:23,640
like Krita supported an openGL based canvas, GPU accelerated before Photoshop. CMYK for ages. We

33
00:02:23,640 --> 00:02:28,040
started implementing more and more fun stuff, more and more useful stuff and then came the

34
00:02:28,040 --> 00:02:33,800
big break. We started listening to our users we had these sessions. Development Sprints,

35
00:02:33,800 --> 00:02:38,960
where Krita´s developers and contributors come together, mostly we work together discuss

36
00:02:38,960 --> 00:02:44,000
what we are going to do in the next year ever 
since our sprint at Blender Studios... ...and

37
00:02:44,000 --> 00:02:49,560
then we ask the artists to "Demo" working with 
Krita we just asked them and... here's an hour

38
00:02:49,560 --> 00:02:55,000
for you and we are going to watch you paint, hope 
you don't mind, you can say anything you want,

39
00:02:55,000 --> 00:03:00,320
complain about everything you've got to complaint 
about. Anything goes, and us the developers, are not

40
00:03:00,320 --> 00:03:03,960
allowed to talk back. We are not allowed to help you either, because we want to see what

41
00:03:03,960 --> 00:03:11,080
the problems. And that kind of user observation really helped us improve cre by leaps and bounds.

42
00:03:11,080 --> 00:03:18,000
What has been the most creative use of Krita that made you think...

43
00:03:18,000 --> 00:03:20,680
Wow! i never thought about that!

44
00:03:20,680 --> 00:03:27,120
the most out there use of Krita? I guess that's the person who wanted to use Krita's high bit

45
00:03:27,120 --> 00:03:34,120
depth capabilities for making star maps. Used enormous astronomical photographs and

46
00:03:34,120 --> 00:03:43,680
wanted to work with them and honestly, I would have--,  (well I actually did say that), use VIPS for that, But it was quite out there use.

47
00:03:43,880 --> 00:03:49,440
Many users might wonder, what motivates the team behind Krita?

48
00:03:50,440 --> 00:03:56,520
What motivates us? I guess a selfish and and unselfish reason. The selfish reason is

49
00:03:56,520 --> 00:04:02,320
this is the most fun work I've ever done in my life. I've worked for all kinds of companies at least

50
00:04:02,320 --> 00:04:09,000
six or seven and at only one of those companies we actually released the software I was developing  on,

51
00:04:09,000 --> 00:04:15,600
some companies just develop software to spend money it's crazy but Krita gets out in

52
00:04:15,600 --> 00:04:21,920
the hand of users and that's where the unselfish part comes in, because these users tell us the

53
00:04:21,920 --> 00:04:29,200
sweetest things, like that time I got an e-mail from someone in Malaysia. She was told by her parents

54
00:04:29,200 --> 00:04:33,840
that "girls don't need to draw, so no, we are not getting you drawing software" her brother

55
00:04:33,840 --> 00:04:39,520
gave her a drawing tablet, so she discovered 
Krita and she could start making Art and she

56
00:04:39,520 --> 00:04:46,120
mailed me in tones of utter ecstasy: " I can make 
art thank you, thank you, thank you "and that's just

57
00:04:46,120 --> 00:04:51,800
so motivating give us an incredible feeling so if you like Krita think of telling us you like it

58
00:04:51,800 --> 00:04:58,320
because normally we only see bug reports. Bug reports are useful, if you make bug reports, thank you!

59
00:04:58,320 --> 00:05:03,080
But sometimes they can be a little bit demotivating because there are so many of them.

60
00:05:03,200 --> 00:05:09,400
Can you tell us more about the commercial use of Krita?

61
00:05:09,400 --> 00:05:16,240
Commercial use of Krita. It's of course totally fine. That's what we explicitly say in all our

62
00:05:16,240 --> 00:05:22,280
Frequently Asked question lists (FAQ) in the about box everywhere. But commercial

63
00:05:22,280 --> 00:05:29,160
use actually happens. The first time I heard of it was when we were presenting Krita at SIGGRAPH in Vancouver.

64
00:05:29,160 --> 00:05:35,694
We had a booth next to blender thanks to Ton Roosendaal's really useful help, we were like...

65
00:05:35,694 --> 00:05:41,600
there's so much, so much interest, so many people want to speak to us and then this guy came

66
00:05:41,600 --> 00:05:49,680
up to me and he said don't tell anyone else at least for now but -psst- we've been using Krita

67
00:05:49,680 --> 00:05:55,200
for the introduction for "Little, Big", which was an animation movie I think by Disney,

68
00:05:55,200 --> 00:06:01,560
I'm not sure anymore, so that was the first I heard of the commercial use of Krita and that was 2014, I think.

69
00:06:01,560 --> 00:06:06,480
Ever since, we've been getting sponsoring we've been in touch with people

70
00:06:06,480 --> 00:06:14,360
using Krita commercially for games, for movies, for books, and that's just highly stimulating.

71
00:06:14,360 --> 00:06:23,240
Ok, So We want to help Krita, How is Krita funded, How sustainable is for the future?

72
00:06:23,240 --> 00:06:28,160
We already talked about that a bit. Basically there are two entities. On one hand there's the

73
00:06:28,160 --> 00:06:36,600
nonprofit Krita Foundation, on the other hand there is the for profit Halla Rempt software, that's me.

74
00:06:36,600 --> 00:06:44,000
and we split up activities between the two. The foundation solicits donations which means it's tax

75
00:06:44,000 --> 00:06:50,400
exempt, Halla Rempt software sells DVDs and stuff, but of course that's hardly any income anymore

76
00:06:50,400 --> 00:06:58,080
these days because we've got these great tutorials on YouTube ❤. It sells Krita in various app stores.

77
00:06:58,080 --> 00:07:06,400
The two income streams combined, the Fund onetime donations, income from stores is enough to sponsor

78
00:07:06,400 --> 00:07:13,000
our currently sponsored team of developers and Ramon's videos so that's quite sustainable at

79
00:07:13,000 --> 00:07:21,160
least we've been doing this since 2015 with 
stores added in 2017 so we're doing a good job.

80
00:07:21,160 --> 00:07:28,520
How the community has been supporting Krita and how Krita has been supporting his community?

81
00:07:28,520 --> 00:07:33,360
Way back! when Krita was just a hobby for everyone who was involved,

82
00:07:33,360 --> 00:07:39,120
most of us didn't have access to digital drawing tablets. I started working in Krita because I got

83
00:07:39,120 --> 00:07:47,362
one for my birthday but it started failing so we did a fundraiser to get two Wacom tablets an art pen!

84
00:07:47,362 --> 00:07:54,280
so that was when we noticed that especially free software community ,there wasn´t even artists

85
00:07:54,280 --> 00:08:03,560
back then, were really supportive, and bit later on, That was Lukas, we had this Slovakian

86
00:08:03,560 --> 00:08:09,800
code student, who went on to become a regular contributor, he did even did his thesis on Krita

87
00:08:09,800 --> 00:08:15,400
brush engines. He got 10 marks out of 10 for it because it was brilliant work and his professors

88
00:08:15,400 --> 00:08:21,520
were so amazed! that their student actually did stuff that was used in the real world!

89
00:08:21,520 --> 00:08:28,320
At the same same time we were like, we were porting Krita from Qt3 to Qt4 ,we were rewriting everything

90
00:08:28,320 --> 00:08:34,720
nothing works, everything is too "buggy" for words it's really horrible! at that point Lukas said:

91
00:08:34,720 --> 00:08:40,960
"My last three months at University are supposed to be an internship. I have to do stuff relevant,

92
00:08:40,960 --> 00:08:47,160
useful, I would prefer to be paid for it a little 
bit." And because Krita at that time was so incredibly

93
00:08:47,160 --> 00:08:55,720
buggy, and we were fighting as still as volunteers as hobby coders to get Krita into a useful state, let's

94
00:08:55,720 --> 00:09:04,480
fund Lukas to just fix bugs for 3 months. It was a HUGE success!. Initially Lukas mainly wanted some

95
00:09:04,480 --> 00:09:12,840
money to pay his rent but we could pay him for then, for us, a decent amount of money every month,

96
00:09:12,840 --> 00:09:20,400
with the result of the fundraiser, so many artists, so many enthusiastic people, supported Krita.

97
00:09:20,400 --> 00:09:27,520
Three months became six months, and then a bit later on, 
Dmitry Kazakov has been doing brilliant work ever

98
00:09:27,520 --> 00:09:34,160
since his first GSOC with Krita. He was going to finish University and I did not want him to

99
00:09:34,160 --> 00:09:39,960
take another job and lose him for the project. So we started doing some regular fundraisers

100
00:09:39,960 --> 00:09:46,400
which actually got us a lot of publicity and was exhausting, but fun to do and well Dmitry is still

101
00:09:46,400 --> 00:09:49,880
full-time working on Krita, as sponsored developer, 
so that worked.

102
00:09:50,000 --> 00:09:57,360
Could you remember a time where Krita´s community came to help the Development of Krita?

103
00:09:57,600 --> 00:10:01,600
Worst moment was when the Dutch tax office...

104
00:10:01,600 --> 00:10:07,040
noticed that... let's skip the complicated stuff it's giving me a headache, anyway

105
00:10:07,040 --> 00:10:10,720
they wanted to have €24,000 € in one go from the

106
00:10:10,720 --> 00:10:14,800
Krita foundation, money which we didn't have when we told

107
00:10:14,800 --> 00:10:21,000
the world about that. The world ran to support 
us! which was great we actually actually from

108
00:10:21,000 --> 00:10:27,320
that moment on we've never looked back we've got a supportive community and we've got a development

109
00:10:27,320 --> 00:10:35,280
fund, which I urge you to join, sorry for the plug, uh we do sell Krita in various App stores. App stores

110
00:10:35,280 --> 00:10:43,480
are absolute hell to work on, but it does pay ours monthly, thank you community for supporting us!

111
00:10:43,480 --> 00:10:50,840
How Krita is helping  users,  in learning the software or even coding the software?

112
00:10:51,280 --> 00:10:56,520
We are trying to make Krita accessible or even learnable for newcomers,

113
00:10:56,520 --> 00:11:03,640
but in the first place of course, we simply try to keep existing 
user interface standards. We're not trying to do

114
00:11:03,640 --> 00:11:09,488
weird new stuff. We just want people to be able to find everything in the place they are used to.

115
00:11:09,488 --> 00:11:15,920
If you've used a computer for the past 5, 10, 20 or 30 years, then nothing in Krita will really

116
00:11:15,920 --> 00:11:22,600
surprise you that makes Krita in itself usable. We are of course also working together with our

117
00:11:22,600 --> 00:11:29,840
user interface (UI) expert Scott Petrovic and he's helping 
us to design new features. Now if you go beyond the

118
00:11:29,840 --> 00:11:36,560
level of the application itself, then documentation is really important. We spend quite a bit of time

119
00:11:36,560 --> 00:11:44,440
and effort on our documentation website: "docs.krita.org" 
doesn't just list all the menu options, also gives

120
00:11:44,440 --> 00:11:51,600
you tutorials about workflow things. Things that might not be apparent if you're a office user of

121
00:11:51,600 --> 00:11:57,280
digital painting applications because let's be clear on this, Digital Painting is a tool

122
00:11:57,840 --> 00:12:05,720
separated from everything else. I mean yeah. I paint analog myself as well

123
00:12:05,720 --> 00:12:12,240
using oil paints and I draw a sketch and sculpt and then when I try to draw using Krita I know

124
00:12:12,240 --> 00:12:18,600
that most of the learnt habits don't transfer to a digital painting application so we have

125
00:12:18,600 --> 00:12:24,920
an extensive website with tutorials, guidelines, 
tips, we also have a great community and thanks

126
00:12:24,960 --> 00:12:34,202
to Raghavendra Kamath (raghukamath) we have a userform website which is called: krita-artists.org , just like Blender Artists

127
00:12:34,202 --> 00:12:38,120
it's totally based on that. That's a recurring theme. Whenever I think we need

128
00:12:38,120 --> 00:12:43,320
something new, I first look at what Ton is been doing for Blender and then I just copy that and I'm not

129
00:12:43,320 --> 00:12:49,040
ashamed to admit it. Our Krita artist forum is a huge resource where people can ask questions

130
00:12:49,040 --> 00:12:54,640
but also find answers to questions that have been asked before. Finally we invest a lot of

131
00:12:54,640 --> 00:13:01,280
time and money, in Ramón series of videos for our Krita channel, so thank you Ramón for working with us!.

132
00:13:01,360 --> 00:13:11,160
How customizable is Krita, and what kind of plugins or extensions can users create or use?

133
00:13:11,840 --> 00:13:18,760
When Krita was really young Cyrille Berger first created the first scripting interface for Krita

134
00:13:18,760 --> 00:13:24,960
to be scripted in JavaScript, Ruby and Python. Wait! that's not actually the first our first scripting

135
00:13:24,960 --> 00:13:30,120
interface. KDE's JavaScript interface. I mean you 
know that pretty much every browser except for

136
00:13:30,120 --> 00:13:39,640
Firefox these days is based on KDE's work on KHTML a KDE based web browser. That came of course with JavaScript engine.

137
00:13:39,640 --> 00:13:47,720
And that JavaScript engine was a separate thingy that we could use to make Krita scriptable,

138
00:13:47,720 --> 00:13:52,560
so that was the very first thing. so we had first had JavaScript then we replaced

139
00:13:52,560 --> 00:14:02,720
that with Kross, which is the thing that made 
Ruby, Python, Etc... possible. Then we added Open GTL

140
00:14:02,720 --> 00:14:09,360
mostly forgotten these days, a one-on-one clone of something used in Hollywood reimplemented that or

141
00:14:09,360 --> 00:14:15,920
rather Cyrille Berger reimplemented that and it could be used 
for lots of things including creating entirely new  colorspaces.

142
00:14:15,920 --> 00:14:22,920
It was really cool, but Cyrille got married and left the project 
and it was basically unmaintainable so we dropped that.

143
00:14:22,920 --> 00:14:29,400
At the same time, we ported Krita to a new version of the KDE Frameworks, and Kross stopped working properly

144
00:14:29,400 --> 00:14:35,760
so we dropped support for that. Fortunately back then Krita had very few users, so nobody was really  impacted.

145
00:14:35,760 --> 00:14:43,920
Then we ported Krita to another version of Qt and KDE, which was a lot of work

146
00:14:43,920 --> 00:14:51,440
One summer out  on the roof terrace when it was really warm, 
I took the python scripting code from KDE's text

147
00:14:51,440 --> 00:14:58,280
editor Kate and copied all of that and fixed it to make Krita scriptable. That's the basis of our

148
00:14:58,280 --> 00:15:05,760
current python scripting API which is a little bit limited, but it's growing and it's still there.

149
00:15:05,760 --> 00:15:11,920
You  can do anything user-interface-ish in it and script however you like. And then, of course,

150
00:15:11,920 --> 00:15:19,200
we had this huge Summer of code project (GSOC) where we 
implemented support for Disney's SeExpr.

151
00:15:19,200 --> 00:15:25,699
It makes really possible to write script, generate stuff that will fill your... your layers with anything you want

152
00:15:25,699 --> 00:15:31,240
We're actually not sure how much it is  used but it's extremely powerful. So yeah! Krita is very

153
00:15:31,240 --> 00:15:37,120
customizable, and even apart from that, most of Krita is plugins. Every tool is a plugin. if you know a

154
00:15:37,120 --> 00:15:43,560
little bit of C++, I mean I didn't know C++ when I started hacking on Krita and I did fine in failing

155
00:15:43,560 --> 00:15:50,560
to do anything useful which in turn attracted other contributors, and krita took off so let's start hacking!

156
00:15:50,880 --> 00:15:56,560
For the last words, Thanks a lot for being available for this interview,

157
00:15:56,560 --> 00:16:02,240
I know you are really busy, so I appreciate it a lot.

158
00:16:02,240 --> 00:16:05,440
What message would you like to send to the Krita´s users?

159
00:16:05,440 --> 00:16:09,000
Thank you Ramón for the interview. A last message for everyone who uses Krita.

160
00:16:09,000 --> 00:16:13,520
Use Krita more! make art, make art, make art! I love to see all the art made with Krita.

161
00:16:13,520 --> 00:16:18,116
Anything you do with Krita is fine with me. bye bye!

162
00:16:18,116 --> 00:16:24,384
Celebrate with us the 25th anniversary of development for Krita

163
00:16:24,384 --> 00:16:32,312
Any feedback is really welcome. Ramon here as usual, saying good bye, have a nice day... or night. Thanks a lot for watching. ❤

