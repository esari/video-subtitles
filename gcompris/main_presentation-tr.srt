1
00:00:00,000 --> 00:00:07,166
GCompris: Yüksek kalitede bir eğitim yazılımı

2
00:00:08,633 --> 00:00:12,533
GCompris, 2-10 yaş arası çocuklar için 180'den fazla etkinlik sunar

3
00:00:13,966 --> 00:00:17,633
Tüm etkinlikler, hitap ettikleri yaş aralığına göre eğlenceli olmaları için tasarlanmışlardır

4
00:00:19,066 --> 00:00:23,633
Burada örneklerle etkinlik kategorilerinin listesini görebilirsiniz

5
00:00:23,633 --> 00:00:28,566
Bilgisayar keşfi: Bir klavyeyi, fareyi, dokunmatik ekranı kullanmayı öğren...

6
00:00:28,566 --> 00:00:31,900
Klavye: Yere düşmeden önce sözcüğü yaz

7
00:00:32,233 --> 00:00:33,900
Fare: Düğmeleri kullanmayı öğren

8
00:00:35,233 --> 00:00:37,233
Dokunmatik ekran/yüzey: Koordinasyon pratiği

9
00:00:38,566 --> 00:00:42,800
Okuma/Yazma: Harfleri tanımak...

10
00:00:45,233 --> 00:00:50,966
... sözcükleri okumak, ...

11
00:00:51,933 --> 00:00:57,566
... ve söz dağarcığını genişletmek.

12
00:00:58,633 --> 00:01:02,800
Matematiğini geliştir

13
00:01:02,800 --> 00:01:06,966
Numaralandırma ve hesaplama alıştırmaları, ölçüm problemlerini çözme

14
00:01:08,566 --> 00:01:12,766
Bilim ve İnsanlık: Deneyler yap, tarihi ve coğrafyayı öğren

15
00:01:18,633 --> 00:01:22,800
Oyunlar oyna: Satranç, dördü hizala, Ovare ve çok daha fazlası...

16
00:01:28,533 --> 00:01:33,566
GCompris'i Windows, Android, macOS, ve Linux için ücretsiz olarak indirebilirsin

17
00:01:38,600 --> 00:01:48,633
GCompris: Bir KDE ürünü
