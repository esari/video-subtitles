1
00:00:00,000 --> 00:00:07,166
GCompris: Kalitate handiko software suite hezigarria

2
00:00:08,633 --> 00:00:12,533
GCompris 2 eta 10 urte arteko haurrentzako 180 jarduera baino gehiagorekin dator

3
00:00:13,966 --> 00:00:17,633
Jarduera guztiak diseinatu diren adin-talderako dibertigarriak dira

4
00:00:19,066 --> 00:00:23,633
Hemen da jarduera-kategorien zerrenda bat adibideekin

5
00:00:23,633 --> 00:00:28,566
Ordenagailua ezagutzea: Ikasi teklatua, sagua, ukimen-pantaila... erabiltzen

6
00:00:28,566 --> 00:00:31,900
Teklatua: Tekleatu hitza hura lurrera iritsi aurretik

7
00:00:32,233 --> 00:00:33,900
Sagua: Ikasi botoiak erabiltzen

8
00:00:35,233 --> 00:00:37,233
Ukipen-pantaila/ukimen-kuxina, koordinazioa praktikatzea

9
00:00:38,566 --> 00:00:42,800
Irakurtzea/Idaztea: Letrak ezagutzea...

10
00:00:45,233 --> 00:00:50,966
... hitzak irakurtzea, ...

11
00:00:51,933 --> 00:00:57,566
... zure hiztegia zabaltzea.

12
00:00:58,633 --> 00:01:02,800
Hobetu zure trebetasuna matematikan

13
00:01:02,800 --> 00:01:06,966
Zenbakitze eta kalkulu trebakuntza, neurketa asmakizunak ebatziz

14
00:01:08,566 --> 00:01:12,766
Zientzia eta Humanitateak: Esperimentuak egitea, historia eta geografia ikastea

15
00:01:18,633 --> 00:01:22,800
Jokoetan jolastea: Xakea, lauko artzain jokoa, Oware eta beste asko...

16
00:01:28,533 --> 00:01:33,566
GCompris doan zama-jaitsi dezakezu Windows, Android, macOS, eta Linux-erako

17
00:01:38,600 --> 00:01:48,633
GCompris: KDEk egina
