1
00:00:02,566 --> 00:00:13,466
Compta els elements - Aprendre a agrupar i comptar objectes

2
00:00:16,400 --> 00:00:22,400
Seleccioneu «Matemàtiques» (l'ovella Eduard) > «Numeració»
i feu clic en l'activitat

3
00:00:33,200 --> 00:00:38,300
Seleccioneu la dificultat

4
00:00:52,133 --> 00:00:57,133
Agrupar és una part important d'aprendre a comptar

5
00:00:57,133 --> 00:01:01,766
Ajuda a no oblidar cap objecte
i a comptar-los només una vegada

6
00:01:51,100 --> 00:01:57,533
Una vegada s'han respost totes les preguntes
d'un nivell, s'inicia el nivell següent

7
00:01:57,533 --> 00:02:01,700
Podeu triar el nivell
fent clic en el número de nivell

8
00:02:09,133 --> 00:02:25,533
Esperem que gaudiu d'esta activitat!
