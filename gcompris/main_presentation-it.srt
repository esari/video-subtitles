1
00:00:00,000 --> 00:00:07,166
GCompris è una collezione di software didattico di alta qualità

2
00:00:08,633 --> 00:00:12,533
GCompris offre più di 180 attività per bambini dai 2 ai 10 anni

3
00:00:13,966 --> 00:00:17,633
Tutte le attività sono divertenti per la fascia di età a cui sono pensate

4
00:00:19,066 --> 00:00:23,633
Ecco un elenco di categorie di attività con esempi

5
00:00:23,633 --> 00:00:28,566
Scoperta del computer: impara a utilizzare tastiera, mouse, touchscreen...

6
00:00:28,566 --> 00:00:31,900
Tastiera: digita la parola prima che raggiunga il suolo

7
00:00:32,233 --> 00:00:33,900
Mouse: impara a utilizzare i pulsanti

8
00:00:35,233 --> 00:00:37,233
Pratica di coordinazione schermo tattile/touchpad

9
00:00:38,566 --> 00:00:42,800
Lettura/Scrittura: riconosci le lettere...

10
00:00:45,233 --> 00:00:50,966
... leggi le parole, ...

11
00:00:51,933 --> 00:00:57,566
...ed espandi il tuo vocabolario.

12
00:00:58,633 --> 00:01:02,800
Migliora le tue abilità matematiche

13
00:01:02,800 --> 00:01:06,966
Esercizi di numerazione e calcolo, risoluzione di problemi di misurazione

14
00:01:08,566 --> 00:01:12,766
Scienze e discipline umanistiche: realizza esperimenti, impara la storia e la geografia

15
00:01:18,633 --> 00:01:22,800
Giochi: scacchi, forza quattro, Oware e molti altri...

16
00:01:28,533 --> 00:01:33,566
Puoi scaricare GCompris gratuitamente per Windows, Android, macOS e Linux

17
00:01:38,600 --> 00:01:48,633
Gcompris: realizzato da KDE
