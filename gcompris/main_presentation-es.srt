1
00:00:00,000 --> 00:00:07,166
GCompris: Un paquete de software educativo de alta calidad

2
00:00:08,633 --> 00:00:12,533
GCompris contiene más de 180 actividades para niños de 2 a 10 años

3
00:00:13,966 --> 00:00:17,633
Todas las actividades son divertidas para el grupo de edad al que están destinadas

4
00:00:19,066 --> 00:00:23,633
Esta es una lista de categorías de actividades con ejemplos

5
00:00:23,633 --> 00:00:28,566
Descubre el ordenador: Aprende cómo usar el teclado, el ratón, la pantalla táctil...

6
00:00:28,566 --> 00:00:31,900
El teclado: Escribe la palabra antes de que llegue al suelo

7
00:00:32,233 --> 00:00:33,900
El ratón: Aprende a usar los botones

8
00:00:35,233 --> 00:00:37,233
Práctica de coordinación de la pantalla táctil/panel táctil

9
00:00:38,566 --> 00:00:42,800
Lectura y escritura: Reconoce letras...

10
00:00:45,233 --> 00:00:50,966
... lee palabras...

11
00:00:51,933 --> 00:00:57,566
... y amplía tu vocabulario.

12
00:00:58,633 --> 00:01:02,800
Mejora tus habilidades matemáticas

13
00:01:02,800 --> 00:01:06,966
Prácticas de numeración y cálculo, resolución de problemas de medidas

14
00:01:08,566 --> 00:01:12,766
Ciencia y humanidades: Realiza experimentos, aprende historia y geografía

15
00:01:18,633 --> 00:01:22,800
Juega: Ajedrez, Conecta cuatro, Oware y muchos más...

16
00:01:28,533 --> 00:01:33,566
Puedes descargar GCompris gratis para Windows, Android, macOS y Linux

17
00:01:38,600 --> 00:01:48,633
GCompris: creado por KDE
