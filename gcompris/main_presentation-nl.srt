1
00:00:00,000 --> 00:00:07,166
GCompris: een educatieve softwaresuite van hoge kwaliteit

2
00:00:08,633 --> 00:00:12,533
GCompris komt met 180 activiteiten voor kinderen van 2 tot 10 jaar

3
00:00:13,966 --> 00:00:17,633
Alle activiteiten geven plezier aan de leeftijdsgroep waarvoor ze zijn ontworpen

4
00:00:19,066 --> 00:00:23,633
Hier is een lijst met categorieën van activiteiten met voorbeelden

5
00:00:23,633 --> 00:00:28,566
Ontdek de computer: leer het gebruiken van een toetsenbord, muis, aanraakscherm...

6
00:00:28,566 --> 00:00:31,900
Toetsenbord: typ het woord voordat het de grond bereikt

7
00:00:32,233 --> 00:00:33,900
Muis: leer de muisknoppen te gebruiken

8
00:00:35,233 --> 00:00:37,233
Coördinatie oefening mat aanraakscherm/aanraakpad 

9
00:00:38,566 --> 00:00:42,800
Lezen/schrijven: herkennen van letters...

10
00:00:45,233 --> 00:00:50,966
... woorden lezen, ...

11
00:00:51,933 --> 00:00:57,566
... en jouw woordenschat uitbreiden.

12
00:00:58,633 --> 00:01:02,800
Jouw wiskundige vaardigheden verbeteren

13
00:01:02,800 --> 00:01:06,966
Trainen van tellen en rekenen, problemen met metingen oplossen

14
00:01:08,566 --> 00:01:12,766
Natuurkunde en geesteswetenschappen: voer experimenten uit, leer over geschiedenis en aardrijkskunde

15
00:01:18,633 --> 00:01:22,800
Spellen spelen: schaken, vier op een rij, Oware en nog veel meer...

16
00:01:28,533 --> 00:01:33,566
Je kunt GCompris zonder kosten downloaden voor Windows, Android, macOS en Linux

17
00:01:38,600 --> 00:01:48,633
Gcompris: gemaakt door KDE
