1
00:00:00,000 --> 00:00:07,166
GCompris: En högkvalitativ pedagogisk programvarusvit

2
00:00:08,633 --> 00:00:12,533
GCompris levereras med mer än 180 aktiviteter för barn från 2 till 10 år gamla

3
00:00:13,966 --> 00:00:17,633
Alla aktiviteter är roliga för åldersgruppen de är gjorda för

4
00:00:19,066 --> 00:00:23,633
Här är en lista över aktivitetskategorier med exempel

5
00:00:23,633 --> 00:00:28,566
Upptäck datorn: Lär dig hur man använder tangentbord, mus, pekskärm ...

6
00:00:28,566 --> 00:00:31,900
Tangentbord: Skriv in ordet innan det når marken

7
00:00:32,233 --> 00:00:33,900
Mus: Lär dig att använda knapparna

8
00:00:35,233 --> 00:00:37,233
Koordinationsövning med pekskärm eller tryckplatta

9
00:00:38,566 --> 00:00:42,800
Läsa och skriva: Känna igen bokstäver ...

10
00:00:45,233 --> 00:00:50,966
... läsa ord, ...

11
00:00:51,933 --> 00:00:57,566
... och utöka ditt ordförråd.

12
00:00:58,633 --> 00:01:02,800
Förbättra dina matematikkunskaper

13
00:01:02,800 --> 00:01:06,966
Räkning och beräkningsträning, lösa mätproblem

14
00:01:08,566 --> 00:01:12,766
Naturvetenskap och humaniora: Utför experiment, lär dig historia och geografi

15
00:01:18,633 --> 00:01:22,800
Spela spel: Schack, Arrangera fyra, Oware och många fler ...

16
00:01:28,533 --> 00:01:33,566
Du kan ladda ner GCompris gratis för Windows, Android, macOS och Linux

17
00:01:38,600 --> 00:01:48,633
Gcompris: Skapat av KDE
